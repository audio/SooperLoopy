//
// SooperLoopy
//
// Copyright 2019-2020 Fernando Lopez-Lezcano
// All Rights Reserved
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// Custom classes required:
//
//   AmbiXPanRad5H5P
//   AmbiXPanRad3H3P (panner for reverb)
//   ZitaRevEngine (reverberation core - zita_rev_engin*)
//   ReverbDecoderACNSN3D3H3P (ambisonics decoder for reverb)

s = Server.default.boot;

m = MIDIClient.init;

// X-Touch Compact
//
// midi channel for X-Touch Compact
~xtouch_ch = 0;
// midi channel for X-Touch Compact
~xtouch_global_ch = 1;

~midi_out = MIDIOut.newByName("X-TOUCH COMPACT", "X-TOUCH COMPACT MIDI 1");
~midi_out.latency = 0;
MIDIIn.connectAll(true);
~xtouch = MIDIOut.findPort("X-TOUCH COMPACT", "X-TOUCH COMPACT MIDI 1");
~midi_out.connect(~xtouch);

(
// start servers
Routine({

	// a signal for startup synchronization
	var waiter = Condition.new;

	//
	// start a routine that starts all servers (supercollider, midi, swing)
	//
	waiter.test = false;

	Routine({
		// make sure supernova is not connected to any jack ports by default
		"SC_JACK_DEFAULT_INPUTS".setenv("trash");
		"SC_JACK_DEFAULT_OUTPUTS".setenv("trash");

		// boot the supercollider server if necessary
		"<-- booting supernova...\n".postf();
		Server.program = "supernova";
		o = Server.default.options;
		// number of threads
		o.threads = 3;
		// default is 1024
		o.maxNodes = 2048;
		// default is 8192
		o.memSize = 131072;
		// default is 1024
		o.numBuffers = 2048;
		// default is 64
		o.numWireBufs = 128;
		// number of i/o channels
		// 36 for 5th order ambisonics output, 9 for second order outputs (8 extra)
		o.numOutputBusChannels = 36 + 9 + 8;
		o.numInputBusChannels  = 28;
		// boot it if necessary
		if (s.notNil and: { s.serverRunning }, {
			"--> supernova is already running\n".postf();
		}, {
			s = Server.local.bootSync;
			"--> supernova is ready\n".postf();
		});
		// groups for supernova
		~mod_group = ParGroup.new(s);
		~gen_group = ParGroup.new(~mod_group, \addAfter);
		~rev_group = ParGroup.new(~gen_group, \addAfter);

		// initialize midi
		"<-- initializing MIDI...\n".postf();
		m = MIDIClient.init;
		while ({ m.destinations.isNil }, {	0.5.wait; });
		"--> MIDI initialized\n".postf();

		waiter.test = true;
		waiter.signal;

	}).play(SystemClock);

	// wait until server startup is complete
	waiter.wait;
	"--> all servers running\n".postf();

	// end of the main startup Routine
}).play(SystemClock);

// end of block that starts all the servers
)

(

//
// Looper instruments
//

// global playback amplitude boos
~master_level_state = 18.dbamp;

// lowest displayed level for all meters (in dB)
~meter_low_level = -65;

// global envelope attack and decay
~env_att = 0.01;
~env_dec = 0.02;

// lag for control parameters
~lag = 0.02;

// medium lag
~lag_med = 0.06;

// lag for slow changes
~lag_long = 0.5;

// maximum delay for doppler delay lines
~max_delay = 2.0;

// speed of sound for delay calculation
~speed_of_sound = 340.0;

// 3D Lissajous trajectories
//
// http://en.wikipedia.org/wiki/Lissajous_curve
//
// control signals are independent of the panners to make the
// trajectories coherent for each looper as its instruments are
// started or stopped (ie: a new instance of a particular looper
// starts where the previous one left off)

~trajectory = SynthDef(\trajectory, { | looper, bus = 0, freq = 0.1,
	// frequency multipliers
	// initial
	x_k_i = 1.0, y_k_i = 1.0, z_k_i = 1.0,
	// offset
	x_k = 0.0, y_k = 0.0, z_k = 0.0,
	// speed multiplier
	speed = 1.0,
	// vertical speed multiplier
	z_speed_mult = 0.5,
	// phase
	x_phase = 0, y_phase = 0, z_phase = 0,
	// individual coordinate scalers
	x_scale = 1.0, y_scale = 1.0, z_scale = 1.0,
	// scale delay for doppler (does not modify distance)
	doppler_scale = 1.0,
	// global coordinate scaler (ie: size of curve)
	size = 10.0,
	// minimum distance
	min_dist = 0.25,
	// noise speed multiplier
	noi_mult = 1.0,
	// global azimuth and elevation offsets
	az_offset = 0.0, el_offset = 0.0,
	// x/y plane rotation direction
	direction = 1.0,
	// select full sphere or hemisphere rendering
	hemisphere = 1,
	// power exponent for direct and reverb
	direct_power = 1.0, reverb_power = 1.2 |
	//
	var imp;
	var l_size = Lag.kr(size, ~lag_long);
	// modulation noise
	var x_noi = LFNoise2.kr(0.25 * noi_mult).linlin(-1, 1, 0.9, 1.1);
	var y_noi = LFNoise2.kr(0.125 * noi_mult).linlin(-1, 1, 0.9, 1.1);
	var z_noi = LFNoise2.kr(0.0625 * noi_mult).linlin(-1, 1, 0.9, 1.1);
	// coordinates of lissajous curve (phase = 0 generates a circle)
	var x = SinOsc.kr((freq * speed * x_noi) * (x_k_i + x_k), x_phase.mod(2pi)) * x_scale * l_size;
	var y = SinOsc.kr((freq * speed * y_noi) * (y_k_i + y_k), (y_phase + (pi/2)).mod(2pi)) * y_scale * l_size;
	var z = SinOsc.kr((freq * speed * z_noi * z_speed_mult) * (z_k_i + z_k), z_phase.mod(2pi)) * z_scale * l_size;
	// convert to polar coordinates
	var dist = sqrt((x*x)+(y*y)+(z*z));
	var az = Wrap.kr(atan2(y, x) + (az_offset / 180 * pi), -pi, pi) * direction;
	var el = Wrap.kr(asin(z / dist) + (el_offset / 180 * pi), -pi/2, pi/2);
	// calculate signal delay for doppler
	var delay = dist / ~speed_of_sound;
	// calculate distance attenuations (limit minimum radius to 1.0m)
	var direct_gain = 1.0 / (dist.max(min_dist) ** direct_power);
	var reverb_gain = 1.0 / (dist.max(min_dist) ** reverb_power);
	// send everything out
	Out.kr(bus, [
		az,
		Select.kr(hemisphere.max(1), [el, abs(el)]),
		dist,
		delay,
		direct_gain,
		reverb_gain,
	]);
	// send x/y of movement to display
	imp = Impulse.kr(10);
	SendReply.kr(imp, '/loop_trajectory', [looper, x, y, size]);
}).send(s);

// Loop recorder

SynthDef(\loop_rec) { | input = 0, output = 0, bufindex, bufnum, rec = 0, overdub = 0, start = 0, end,
	source_A = 1.0, source_B = 1.0 |
	var imp, delimp;
	var in = SoundIn.ar(input, mul: Lag.kr(source_A, ~lag)) + SoundIn.ar(input + 1, mul: Lag.kr(source_B, ~lag));
	// overall envelope
	var env = EnvGen.ar(Env.asr(~env_att, 1, ~env_dec, 'welch'), rec, doneAction: 2);
	// envelope that defines the part of the buffer which we merge when overdubing
	var over = EnvGen.ar(Env.linen(attackTime: 0.0, sustainTime: (end - start) / SampleRate.ir, releaseTime: 0.0, level: overdub));
	// ramp that tracks the current sample number being recorded
	var phase = Line.ar(start, BufFrames.kr(bufnum), (BufFrames.kr(bufnum) - start) /  SampleRate.ir);
	var sig = RecordBuf.ar(in * env, bufnum, loop: 0, offset: start, recLevel: 1.0, preLevel: over);
	// do not send recorder to output....
	// Out.ar(output, in * env);
	// send frame number when recording stops
	SendReply.kr(1 - rec, '/rec_stop', [bufindex, bufnum, phase, overdub], 0);
}.send(s);

// Loop player

SynthDef(\loop_play, { | output, reverb, bufindex, bufnum, end, gate, wait = 0,
	master = 1.0, mute = 1.0, level, rate, sign, location = 0, vector, reverb_amount = 0.005,
	// spectral processor parameters
	//
	balance = 1.0,
	imag = 0,
	// conformal map:
	// modulation frequency
	mod_freq,
	// modulation amplitude
	mod_amp = 0.9999,
	// bin scramble
	scram_wipe = 0.1, scram_width = 0.2, scram_freq = 6.8,
	// bin shift
	stretch = 1, shift = 0, polarity = 1
	|
	var it, imp, delimp;
	var transp = Lag.kr(rate, ~lag) * sign;
	var phase = Phasor.ar(1, BufRateScale.kr(bufnum) * transp, 0, end, 0);
	var phase1 = Delay1.ar(phase);
	var delta = Select.ar(transp > 0, [phase - phase1, phase1 - phase]);
	var mask = EnvGen.ar(Env.linen(0, end/2, 0, 1.0), gate: gate);
	var end_trig = ToggleFF.ar(wait * (1 - mask) * delta > (end/2));
	var finish = FreeSelf.kr(end_trig);
	var in = BufRd.ar(1, bufnum, phase, loop: 1, interpolation: 4);
	var env = EnvGen.ar(Env.asr(0, 1, ~env_dec, 'welch'), gate, doneAction: 2);
	var doppler, direct, fft_buffer, mod, chain, processed, panner_in;
	var az, el, dist, delay, direct_gain, reverb_gain, direct_m, reverb_m;
	var bal = Lag.kr(balance, ~lag);
	var lev = Lag.kr(level, ~lag);
	var mut = Lag.kr(mute, ~lag);
	var scram = Lag.kr(scram_wipe, ~lag_med);
	var shft = Lag.kr(shift, ~lag_med);
	// get panning vector coordinates and delay
	#az, el, dist, delay, direct_gain, reverb_gain = In.kr(vector, 6);
	// doppler signal
	doppler = DelayC.ar(in, ~max_delay, delay, env);
	//
	// spectral processing
	fft_buffer = LocalBuf.new(2048, 1).clear;
	mod = LFSaw.kr(mod_freq, 0.0, mod_amp);
	// hop 0.5 is harsher, 0.25 smoother
	chain = FFT(fft_buffer, doppler, hop: 0.25, wintype: 1);
	chain=PV_ConformalMap(chain, mod, imag);
	chain=PV_BinScramble(chain, scram, scram_width, Dust.kr(scram_freq));
	//
	// PV_binshift: polarity introduces weird artifacts (output with level == 0!)
	// this happens with polarity > 0, negative polarities do not exhibit the problem
	// fft buffer size does shift the problem to lower frequencies but the effect is
	// more correct and not so interesting. Using an external or local buffer does
	// not make a difference
	//
	chain=PV_BinShift(chain, stretch, shft * polarity, interp: 0);
	processed = IFFT(chain);
	//
	// pan in ambisonics with distance attenuation
	panner_in = ((doppler * bal) + (processed * (1.0 - bal))) * Lag.kr(master, ~lag);
	direct = AmbiXPanRad5H5P.ar(panner_in, az.mod(2*pi), el.mod(2*pi), 1.0);
	direct_m = direct * lev * mut;
	reverb_m = direct[0 .. 15] * lev * mut;
	// send signal out (level control should be here to mask PV_BinShift problems)
	Out.ar(output, direct_m * direct_gain);
	// send reverb
	Out.ar(reverb, reverb_m * reverb_gain * reverb_amount);
	// trigger end of loop gui indicator
	SendReply.kr(T2K.kr(delta / end > 0), '/do_loop', [bufindex, bufnum], 0);
	// report end of looper
	SendReply.kr(T2K.kr(end_trig), '/looper_done', [bufindex, bufnum], 0);
	// send location report
	SendReply.kr(location, '/play_phase', [bufindex, bufnum, phase], 0);
	// send level of output signal to meters and buffer playback pointer
	imp = Impulse.kr(10);
	delimp = Delay1.kr(imp);
	// send RMS of output signal, but peak value before the mute so that
	// we can get feedback of the loop contents even while its output level is zero
	SendReply.kr(imp, '/play_level', [bufindex, bufnum,
		Amplitude.kr(direct_m[0] * direct_gain),
		max(K2A.ar(Peak.ar(direct[0] * direct_gain, delimp).lag(0, 3)),
			K2A.ar(Peak.ar(direct_m[0] * direct_gain, delimp).lag(0, 3))),
		phase / end]);
	// test az/el reporting
	// it = Impulse.kr(3);
	// SendReply.kr(it, '/play_parameters', [bufindex, az, el, delay]);
}).send(s);

SynthDef(\loop_play_2o, { | output, reverb, bufindex, bufnum, end, gate, wait = 0,
	master = 1.0, mute = 1.0, level, rate, sign, location = 0, vector, reverb_amount = 0.005,
	// spectral processor parameters
	//
	balance = 1.0,
	imag = 0,
	// conformal map:
	// modulation frequency
	mod_freq,
	// modulation amplitude
	mod_amp = 0.9999,
	// bin scramble
	scram_wipe = 0.1, scram_width = 0.2, scram_freq = 6.8,
	// bin shift
	stretch = 1, shift = 0, polarity = 1
	|
	var it, imp, delimp;
	var transp = Lag.kr(rate, ~lag) * sign;
	var phase = Phasor.ar(1, BufRateScale.kr(bufnum) * transp, 0, end, 0);
	var phase1 = Delay1.ar(phase);
	var delta = Select.ar(transp > 0, [phase - phase1, phase1 - phase]);
	var mask = EnvGen.ar(Env.linen(0, end/2, 0, 1.0), gate: gate);
	var end_trig = ToggleFF.ar(wait * (1 - mask) * delta > (end/2));
	var finish = FreeSelf.kr(end_trig);
	var in = BufRd.ar(1, bufnum, phase, loop: 1, interpolation: 4);
	var env = EnvGen.ar(Env.asr(0, 1, ~env_dec, 'welch'), gate, doneAction: 2);
	var doppler, direct, fft_buffer, mod, chain, processed, panner_in;
	var az, el, dist, delay, direct_gain, reverb_gain, direct_m, reverb_m;
	var bal = Lag.kr(balance, ~lag);
	var lev = Lag.kr(level, ~lag);
	var mut = Lag.kr(mute, ~lag);
	var scram = Lag.kr(scram_wipe, ~lag_med);
	var shft = Lag.kr(shift, ~lag_med);
	// get panning vector coordinates and delay
	#az, el, dist, delay, direct_gain, reverb_gain = In.kr(vector, 6);
	// doppler signal
	doppler = DelayC.ar(in, ~max_delay, delay, env);
	//
	// spectral processing
	fft_buffer = LocalBuf.new(2048, 1).clear;
	mod = LFSaw.kr(mod_freq, 0.0, mod_amp);
	// hop 0.5 is harsher, 0.25 smoother
	chain = FFT(fft_buffer, doppler, hop: 0.25, wintype: 1);
	chain=PV_ConformalMap(chain, mod, imag);
	chain=PV_BinScramble(chain, scram, scram_width, Dust.kr(scram_freq));
	//
	// PV_binshift: polarity introduces weird artifacts (output with level == 0!)
	// this happens with polarity > 0, negative polarities do not exhibit the problem
	// fft buffer size does shift the problem to lower frequencies but the effect is
	// more correct and not so interesting. Using an external or local buffer does
	// not make a difference
	//
	chain=PV_BinShift(chain, stretch, shft * polarity, interp: 0);
	processed = IFFT(chain);
	//
	// pan in ambisonics with distance attenuation
	panner_in = ((doppler * bal) + (processed * (1.0 - bal))) * Lag.kr(master, ~lag);
	direct = AmbiXPanRad5H5P.ar(panner_in, az.mod(2*pi), el.mod(2*pi), 1.0);
	direct_m = direct * lev * mut;
	reverb_m = direct[0 .. 15] * lev * mut;
	// send signal out (level control should be here to mask PV_BinShift problems)
	Out.ar(output, direct_m[0 .. 8] * direct_gain);
	// send reverb
	Out.ar(reverb, reverb_m * reverb_gain * reverb_amount);
	// trigger end of loop gui indicator
	SendReply.kr(T2K.kr(delta / end > 0), '/do_loop', [bufindex, bufnum], 0);
	// report end of looper
	SendReply.kr(T2K.kr(end_trig), '/looper_done', [bufindex, bufnum], 0);
	// send location report
	SendReply.kr(location, '/play_phase', [bufindex, bufnum, phase], 0);
	// send level of output signal to meters and buffer playback pointer
	imp = Impulse.kr(10);
	delimp = Delay1.kr(imp);
	// send RMS of output signal, but peak value before the mute so that
	// we can get feedback of the loop contents even while its output level is zero
	SendReply.kr(imp, '/play_level', [bufindex, bufnum,
		Amplitude.kr(direct_m[0] * direct_gain),
		max(K2A.ar(Peak.ar(direct[0] * direct_gain, delimp).lag(0, 3)),
			K2A.ar(Peak.ar(direct_m[0] * direct_gain, delimp).lag(0, 3))),
		phase / end]);
	// test az/el reporting
	// it = Impulse.kr(3);
	// SendReply.kr(it, '/play_parameters', [bufindex, az, el, delay]);
}).send(s);

// Input processing

SynthDef(\input, { | input, output,	reverb, master = 1.0, mute = 1.0, level, vector, reverb_amount = 0.005,
	// spectral processor parameters
	//
	balance = 1.0,
	imag = 0,
	// conformal map:
	// modulation frequency
	mod_freq,
	// modulation amplitude
	mod_amp = 0.9999,
	// bin scramble
	scram_wipe = 0.1, scram_width = 0.2, scram_freq = 6.8,
	// bin shift
	stretch = 1, shift = 0, polarity = 1
	|
	var imp, delimp;
	var doppler, direct, fft_buffer, mod, chain, processed;
	var az, el, dist, delay, direct_gain, reverb_gain, signal, panner_in;
	var bal = Lag.kr(balance, ~lag);
	var lev = Lag.kr(level, ~lag);
	var mut = Lag.kr(mute, ~lag);
	var scram = Lag.kr(scram_wipe, ~lag_med);
	var shft = Lag.kr(shift, ~lag_med);
	// get panning vector coordinates and delay
	#az, el, dist, delay, direct_gain, reverb_gain = In.kr(vector, 6);
	signal = SoundIn.ar(input);
	// doppler signal
	doppler = DelayC.ar(signal, ~max_delay, delay);
	//
	// spectral processing
	fft_buffer = LocalBuf.new(2048, 1).clear;
	mod = LFSaw.kr(mod_freq, 0.0, mod_amp);
	chain = FFT(fft_buffer, doppler);
	chain=PV_ConformalMap(chain, mod, imag);
	chain=PV_BinScramble(chain, scram, scram_width, Dust.kr(scram_freq));
	chain=PV_BinShift(chain, stretch, shft * polarity);
	processed = IFFT(chain);
	//
	// pan in ambisonics with distance attenuation
	panner_in = ((doppler * bal) + (processed * (1.0 - bal))) * Lag.kr(master, ~lag);
	direct = AmbiXPanRad5H5P.ar(panner_in * lev * mut, az.mod(2*pi), el.mod(2*pi), 1.0);
	// send signal out
	Out.ar(output, direct * direct_gain);
	// send reverb
	Out.ar(reverb, direct[0 .. 15] * reverb_gain * reverb_amount);
	// send level of raw input signal to meters
	imp = Impulse.kr(10);
	delimp = Delay1.kr(imp);
	SendReply.kr(imp, '/input_level', [input, Amplitude.kr(signal), K2A.ar(Peak.ar(signal, delimp).lag(0, 3))]);
}).send(s);

SynthDef(\input_2o, { | input, output,	reverb, master = 1.0, mute = 1.0, level, vector, reverb_amount = 0.005,
	// spectral processor parameters
	//
	balance = 1.0,
	imag = 0,
	// conformal map:
	// modulation frequency
	mod_freq,
	// modulation amplitude
	mod_amp = 0.9999,
	// bin scramble
	scram_wipe = 0.1, scram_width = 0.2, scram_freq = 6.8,
	// bin shift
	stretch = 1, shift = 0, polarity = 1
	|
	var imp, delimp;
	var doppler, direct, fft_buffer, mod, chain, processed;
	var az, el, dist, delay, direct_gain, reverb_gain, signal, panner_in;
	var bal = Lag.kr(balance, ~lag);
	var lev = Lag.kr(level, ~lag);
	var mut = Lag.kr(mute, ~lag);
	var scram = Lag.kr(scram_wipe, ~lag_med);
	var shft = Lag.kr(shift, ~lag_med);
	// get panning vector coordinates and delay
	#az, el, dist, delay, direct_gain, reverb_gain = In.kr(vector, 6);
	signal = SoundIn.ar(input);
	// doppler signal
	doppler = DelayC.ar(signal, ~max_delay, delay);
	//
	// spectral processing
	fft_buffer = LocalBuf.new(2048, 1).clear;
	mod = LFSaw.kr(mod_freq, 0.0, mod_amp);
	chain = FFT(fft_buffer, doppler);
	chain=PV_ConformalMap(chain, mod, imag);
	chain=PV_BinScramble(chain, scram, scram_width, Dust.kr(scram_freq));
	chain=PV_BinShift(chain, stretch, shft * polarity);
	processed = IFFT(chain);
	//
	// pan in ambisonics with distance attenuation
	panner_in = ((doppler * bal) + (processed * (1.0 - bal))) * Lag.kr(master, ~lag);
	direct = AmbiXPanRad5H5P.ar(panner_in * lev * mut, az.mod(2*pi), el.mod(2*pi), 1.0);
	// send signal out
	Out.ar(output, direct[0 .. 8] * direct_gain);
	// send reverb
	Out.ar(reverb, direct[0 .. 15] * reverb_gain * reverb_amount);
	// send level of raw input signal to meters
	imp = Impulse.kr(10);
	delimp = Delay1.kr(imp);
	SendReply.kr(imp, '/input_level', [input, Amplitude.kr(signal), K2A.ar(Peak.ar(signal, delimp).lag(0, 3))]);
}).send(s);

// Ambisonics 3rd order reverberator
//
// input is 16 channels wide, ACN ordering and SN3D weights
// (originally developed for PiaNOT)

SynthDef(\AmbiRev, { | input, output, gain = 1.0, delay = 0.02, lfx = 200, rt_mult = 1.0, low_rt60 = 3.0, mid_rt60 = 2.0, hf_damping = 6000.0 |
	var spkr1, spkr2, spkr3, spkr4, spkr5, spkr6, spkr7, spkr8, spkr9, spkr10, spkr11, spkr12, spkr13, spkr14, spkr15;
	var zr1, zr2, zr3, zr4, zr5, zr6, zr7, zr8;
	var zr9, zr10, zr11, zr12, zr13, zr14, zr15, zr16;
	var shhh = DC.ar(0);
	// first decode the Ambisonics input over a virtual dome
	#spkr1, spkr2, spkr3, spkr4, spkr5, spkr6, spkr7, spkr8, spkr9, spkr10, spkr11, spkr12, spkr13, spkr14, spkr15 = ReverbDecoderACNSN3D3H3P.ar(
		In.ar(input), In.ar(input + 1), In.ar(input + 2), In.ar(input + 3),
		In.ar(input + 4), In.ar(input + 5), In.ar(input + 6), In.ar(input + 7),
		In.ar(input + 8), In.ar(input + 9), In.ar(input + 10), In.ar(input + 11),
		In.ar(input + 12), In.ar(input + 13), In.ar(input + 14), In.ar(input + 15));
	// connect each virtual source to an input of a zita rev engine instance
	#zr1, zr2, zr3, zr4, zr5, zr6, zr7, zr8 = gain * ZitaRevEngine.ar(
		spkr1, spkr2, spkr3, spkr4, spkr5, spkr6, spkr7, spkr8,
		lfx, low_rt60 * rt_mult, mid_rt60 * rt_mult, hf_damping);
	#zr9, zr10, zr11, zr12, zr13, zr14, zr15, zr16 = gain * ZitaRevEngine.ar(
		spkr9, spkr10, spkr11, spkr12, spkr13, spkr14, spkr15, shhh,
		lfx, low_rt60 * rt_mult * 0.75, mid_rt60 * rt_mult * 0.75, hf_damping);
	// encode each output of the zita instances back to Ambisonics
	//                  22.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr1, 22.5 / 180 * pi, 0, 1.0));
	//                  67.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr2, 67.5 / 180 * pi, 0, 1.0));
	//                 112.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr3, 112.5 / 180 * pi, 0, 1.0));
	//                 157.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr4, 157.5 / 180 * pi, 0, 1.0));
	//                -157.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr5, -157.5 / 180 * pi, 0, 1.0));
	//                -112.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr6, -112.5 / 180 * pi, 0, 1.0));
	//                 -67.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr7, -67.5 / 180 * pi, 0, 1.0));
	//                 -22.5000         0    3.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr8, -22.5 / 180 * pi, 0, 1.0));
	//                  30.0000   40.0000    2.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr9, 30 / 180 * pi, 40 / 180 * pi, 1.0));
	//                  90.0000   40.0000    2.2300
	Out.ar(output, AmbiXPanRad3H3P.ar(zr10, 90 / 180 * pi, 40 / 180 * pi, 1.0));
	//                 150.0000   40.0000    2.1100
	Out.ar(output, AmbiXPanRad3H3P.ar(zr11, 150 / 180 * pi, 40 / 180 * pi, 1.0));
	//                -150.0000   40.0000    2.1100
	Out.ar(output, AmbiXPanRad3H3P.ar(zr12, -150 / 180 * pi, 40 / 180 * pi, 1.0));
	//                 -90.0000   40.0000    2.2300
	Out.ar(output, AmbiXPanRad3H3P.ar(zr13, -90 / 180 * pi, 40 / 180 * pi, 1.0));
	//                 -30.0000   40.0000    2.1200
	Out.ar(output, AmbiXPanRad3H3P.ar(zr14, -30 / 180 * pi, 40 / 180 * pi, 1.0));
	//                   0        90.0000    1.4400
	Out.ar(output, AmbiXPanRad3H3P.ar(zr15, 0,  90 / 180 * pi, 1.0));
}).send(s);

// start a looper

~looper_start_playing = { | looper |
	var player, output;
	var order = if (0.5.coin, { 1 }, { 0 });
	if (order == 1, {
		"PLAY: 5th order\n".postf();
		player = \loop_play;
		output = 0;
	}, {
		"PLAY: 2nd order\n".postf();
		player = \loop_play_2o;
		output = 36;
		//player = \loop_play;
		//output = 0;
	});
	~loopers[looper] = Synth(player, [\output, output, \reverb, ~reverb_bus[if (looper > 7, { 1 }, { 0 })].index,
		\bufindex, looper, \bufnum, ~loops[looper].bufnum, \end, ~loop_dur_state[looper],
		\master, ~master_level_state, \level, ~loop_level_state[looper], \balance, ~loop_balance_state[looper], \gate, 1,
		\mute, ~loop_mute_state[looper], \rate, ~loop_rate_state[looper], \sign, ~loop_rate_sign_state[looper],
		\vector, ~pan_control_buses[looper], \wait, ~loop_wait_state[looper],
		// spectral processing
		\mod_freq, (0.21 + 0.06.sum3rand),
		\polarity, if (0.5.coin, { 1 }, { -1 }),
		\shift, ~loop_spectral_bend_state[looper] * ~spectral_max_shift_state,
		\scram_wipe, (~loop_spectral_bend_state[looper] * ~spectral_wipe_scaler_state) + ~spectral_wipe_offset_state],
		~gen_group);
};

// Recorder stopped, start corresponding looper

~rec_end = OSCdef(\rec_stop, { | msg |
	var looper, bufnum, end, overdub;
	// looper index
	looper = msg[3];
	// looper buffer number
	bufnum = msg[4];
	// end of recording (actual end plus envelope decay time in samples)
	end = msg[5] + (~env_dec * s.sampleRate);
	// status of overdub flag
	overdub = msg[6];
	"looper %: buffer %, end % [%], overdub %\n".postf(looper, bufnum,
		end / s.sampleRate,
		~loop_dur_state[looper] / s.sampleRate, overdub);
	// start loop playback
	if (overdub == 0, {
		var player, output;
		var order = if (0.5.coin, { 1 }, { 0 });
		var buf;
		// recording:
		// stop player if already running
		if (~loopers[looper].notNil, {
			~loopers[looper].set(\gate, 0);
			~loopers[looper] = nil;
		});
		// swap buffers
		buf = ~loops[looper];
		~loops[looper] = ~loops[~loop_count];
		~loops[~loop_count] = buf;
		"looper %: new buffer %\n".postf(looper, ~loops[looper].bufnum);
		// set duration of loop to that reported
		~loop_dur_state[looper] = end;
		// start brand new player
		if (order == 1, {
			"PLAY: 5th order\n".postf();
			player = \loop_play;
			output = 0;
		}, {
			"PLAY: 2nd order\n".postf();
			player = \loop_play_2o;
			output = 36;
			//player = \loop_play;
			//output = 0;
		});
		~loopers[looper] = Synth(player, [\output, 0, \reverb, ~reverb_bus[if (looper > 7, { 1 }, { 0 })].index,
			\bufindex, looper, \bufnum, ~loops[looper].bufnum, \end, ~loop_dur_state[looper],
			\master, ~master_level_state, \level, ~loop_level_state[looper], \balance, ~loop_balance_state[looper], \gate, 1,
			\mute, ~loop_mute_state[looper], \rate, ~loop_rate_state[looper], \sign, ~loop_rate_sign_state[looper],
			\vector, ~pan_control_buses[looper], \wait, ~loop_wait_state[looper],
			// spectral processing
			\mod_freq, (0.21 + 0.06.sum3rand),
			\polarity, if (0.5.coin, { 1 }, { -1 }),
			\shift, ~loop_spectral_bend_state[looper] * ~spectral_max_shift_state,
			\scram_wipe, (~loop_spectral_bend_state[looper] * ~spectral_wipe_scaler_state) + ~spectral_wipe_offset_state],
			~gen_group);
		// and blink indicator
		~loop_restart_blink.(looper);
	}, {
		// overdubing
		// update the end of the loop only if overdub was longer than original
		if (end > ~loop_dur_state[looper], {
			"looper %: change end from %\n".postf(looper, ~loop_dur_state[looper] / s.sampleRate);
			~loop_dur_state[looper] = end;
			// update end of playback on current player
			~loopers[looper].set(\end, ~loop_dur_state[looper]);
		});
	});
}, \rec_stop);

// looper done with last loop, wait for retrigger

~looper_done = OSCdef(\looper_done, { | msg |
	var looper = msg[3];
	var buffer = msg[4];
	"--> \looper_done, looper %, buffer %\n".postf(looper, buffer);
	~loopers[looper] = nil;
	if (~loop_sync_state[looper] == 1, {
		// cancel any pending overlap routine as sync has priority
		if (~loop_overlap_routine[looper].notNil, {
			~loop_overlap_routine[looper].stop;
			~loop_overlap_routine[looper] = nil;
		});
		// wait for sync from another loop
		~loop_waiting[looper] = true;
	}, {
		// start a wait for overlap if overlap > 1.0
		//   overlap = 1.0 -> loop repeats
		//   overlap > 1.0 -> loop waits overlap times duration before restarting
		//   overlap < 1.0 -> not implemented yet
		if (~loop_overlap_state[looper] > 1.0, {
			if (~loop_overlap_routine[looper].isNil, {
				~loop_overlap_routine[looper] = Routine({
					var seconds = ~loop_dur_state[looper] / s.sampleRate, overdub;
					var overlap = ~loop_overlap_state[looper];
					// wait for the overlap
					(seconds * (overlap - 1.0)).wait;
					// and restart loop
					~looper_start_playing.(looper);
					"OVERLAP: start looper % after overlap % seconds\n".postf(looper, seconds * (overlap - 1.0));
					~loop_overlap_routine[looper] = nil;
				}).play(SystemClock);
			}, {
				// already waiting?
				"ERROR: a routine is already waiting for an overlap timeout for looper %\n".postf(looper);
			});
		});
	});
	// update gui
	if (~loop_meter_gui[looper].notNil, {
		{
			~loop_meter_gui[looper].value = 0;
			~loop_meter_gui[looper].peakLevel = 0;
		}.defer;
	});
	if (~loop_location_gui[looper].notNil, {
		{
			~loop_location_gui[looper].value = 1;
		}.defer;
	});
	"==> %: looper done\n".postf(looper);
}, \looper_done);

// report back play phase location

~play_phase_updated = Condition.new;

~play_phase = OSCdef(\play_phase, { | msg |
	var looper = msg[3];
	var buffer = msg[4];
	var phase = msg[5];
	"--> %: play phase % in buffer %\n".postf(looper, phase, buffer);
	~loopers[looper].set(\location, 0);
	~loop_phase_state[looper] = phase;
	// notify recording routine
	~play_phase_updated.test = true;
	~play_phase_updated.signal;
}, \play_phase);

// refresh gui at end of loop

~do_loop = OSCdef(\do_loop, { | msg |
	var looper = msg[3];
	// "--> %: loop restart\n".postf(looper);
	// trigger any loops waiting for sync from this looper
	~loop_count.do ({ | w_looper |
		if (w_looper != looper and: { ~loop_sync_source_state[looper] == 1 }, {
			if (~loop_waiting[w_looper], {
				// restart looper
				if (~loop_sync_state[w_looper] == 1, {
					~looper_start_playing.(w_looper);
					"SYNC: start looper % from loop % trigger\n".postf(w_looper, looper);
					~loop_waiting[w_looper] = false;
				});
			});
		});
	});
	// signal start
	~loop_restart_blink.(looper);
}, \do_loop);

// output meter refresh

~play_level = OSCdef(\play_level, { | msg |
	var looper = msg[3];
	Routine({
		// update RMS and peak level of output signal
		if (~loop_meter_gui[looper].notNil, {
			~loop_meter_gui[looper].value = msg[5].ampdb.linlin(~meter_low_level, 0, 0, 1);
			~loop_meter_gui[looper].peakLevel = msg[6].ampdb.linlin(~meter_low_level, 0, 0, 1);
		});
		// update playback location in buffer
		if (~loop_location_gui[looper].notNil, {
			~loop_location_gui[looper].value = msg[7];
		});
	}).play(AppClock);
}, \play_level);

// internal parameter report for testing

~play_parameters = OSCdef(\play_parameters, { | msg |
	var looper = msg[3];
	if (looper == 0, {
		"=> l % params % : % : %\n".postf(msg[3], msg[4], msg[5], msg[6]);
	});
}, \play_parameters);

// input meter refresh

~input_level = OSCdef(\input_level, { | msg |
	Routine({
		var input = msg[3];
		if (~input_meter_gui[input].notNil, {
			~input_meter_gui[input].value = msg[4].ampdb.linlin(~meter_low_level, 0, 0, 1);
			~input_meter_gui[input].peakLevel = msg[5].ampdb.linlin(~meter_low_level, 0, 0, 1);
		});
	}).play(AppClock);
}, \input_level);

// loop trajectory display

~loop_trajectory = OSCdef(\loop_trajectory, { | msg |
	Routine({
		var looper = msg[3];
		var x = msg[4];
		var y = msg[5];
		var size = msg[6];
		if (looper > 15 and: { looper < 18 }, {
			// inputs
			if (~input_trajectory_gui[looper - 16].notNil, {
				~input_trajectory_gui[looper - 16].setXY((x/size/2)+0.5, (y/size/2)+0.5);
			});
		}, {
			// loopers
			if (~loop_trajectory_gui[looper].notNil, {
				~loop_trajectory_gui[looper].setXY((x/size/2)+0.5, (y/size/2)+0.5);
			});
		});
	}).play(AppClock);
}, \loop_trajectory);

// OSCdef.freeAll

//
// set up looper variables
//

// number of loopers
~loop_count = 16;

// number of inputs
~input_count = 2;

// number of layers
~layer_count = 2;

// maximum duration of loops
~loop_max_dur = 120;

// 3d pan controllers
~pan_controllers = Array.fill(~loop_count + ~input_count, { nil });

// 3d pan control buses
//
// transfers x/y/z, offset and delay information from the controllers to the loopers
~pan_control_buses = Array.fill(~loop_count + ~input_count, { Bus.control(s, 6); });

// input processing synths
~inputs = Array.fill(~input_count, { nil });

// loop playback synths
~loopers = Array.fill(~loop_count, { nil });

// loopers waiting for trigger
~loop_waiting = Array.fill(~loop_count, { false });

// reverb bus (3rd order, 16 channels)
~reverb_bus = Array.fill(~layer_count, { Bus.audio(s, 16) });

// reverb synth
~reverb = Array.fill(~layer_count, { nil });

// loop buffers
//
// one for each looper plus one reserved for recording
~loops = Array.newFrom(
	(~loop_count + 1).collect({ | i |
		Buffer.alloc(s, s.sampleRate * ~loop_max_dur);
	});
);

// loop status indicator
//   0 -> inactive loop
//   1 -> active loop
//   2 -> active, indicator light blinking or changing color
~loop_status_state = Array.fill(~loop_count, { 0 });

// current loop duration in samples
~loop_dur_state = Array.fill(~loop_count, { 0.0 });

// loop playback phase and end of recording
~loop_phase_state = Array.fill(~loop_count, { 0.0 });

// loop recording synth
~loop_recorder = Array.fill(~loop_count, { nil });

//
// Support functions
//

// start the pan control instrument

~start_pan_controllers = {
	(~loop_count + ~input_count).do ({ | looper |
		var lisa = Synth(\trajectory, [
			\looper, looper,
			\bus, ~pan_control_buses[looper],
			\speed, 0.025 + 0.01.rrand(0.06),
			\direction, if (0.5.coin, { 1 }, { -1}),
			\x_k_i, 1.0.rrand(1.1),
			// clip max elevation
			\z_scale, 0.75,
			// limit doppler
			// \doppler_scale, 0.5,

			//\x_phase, (pi / 4).rrand(pi / 2),
			//\y_k, 1.5.rrand(2.0),
			//\y_phase, (pi / 4).rrand(pi / 2),
			//\z_k, 1.5.rrand(2.0),
			//\z_phase, (pi / 4).rrand(pi / 2),
		], ~mod_group);
		"started pan controller %\n".postf(looper);
		~pan_controllers.put(looper, lisa);
	});
};

~stop_pan_controllers = {
	(~loop_count + ~input_count).do ({ | index |
		~pan_controllers[index].free;
	});
};

//
// X-Touch Compact MIDI constants
//

// rotary knobs (fader strips)
~xt_cc_knob_A = 10;
~xt_cc_knob_B = 37;

// rotary knob center button (fader strips)
~xt_note_knob_button_A = 0;
~xt_note_knob_button_B = 55;

// restart loop button
~xt_note_fader_button_1_A = 16;
~xt_note_fader_button_1_B = 71;

// reverse loop button
~xt_note_fader_button_2_A = 24;
~xt_note_fader_button_2_B = 79;

// overdub loop button
~xt_note_fader_button_3_A = 32;
~xt_note_fader_button_3_B = 87;

// record loop button
~xt_note_fader_button_4_A = 40;
~xt_note_fader_button_4_B = 95;

// faders
~xt_cc_fader_A = 1;
~xt_cc_fader_B = 28;
//
~xt_cc_fader_9_A = 9;
~xt_cc_fader_9_B = 36;

// shared knob cluster
//
~xt_cc_knob_9_A = 18;
~xt_cc_knob_9_B = 45;
//
~xt_note_knob_button_9_A = 8;
~xt_note_knob_button_9_B = 63;
//
~xt_cc_knob_10_A = 19;
~xt_cc_knob_10_B = 46;
//
~xt_note_knob_button_10_A = 9;
~xt_note_knob_button_10_B = 64;
//
~xt_cc_knob_11_A = 20;
~xt_cc_knob_11_B = 47;
//
~xt_note_knob_button_11_A = 10;
~xt_note_knob_button_11_B = 65;
//
~xt_cc_knob_12_A = 21;
~xt_cc_knob_12_B = 48;
//
~xt_note_knob_button_12_A = 11;
~xt_note_knob_button_12_B = 66;
//
~xt_cc_knob_13_A = 22;
~xt_cc_knob_13_B = 49;
//
~xt_note_knob_button_13_A = 12;
~xt_note_knob_button_13_B = 67;
//
~xt_cc_knob_14_A = 23;
~xt_cc_knob_14_B = 50;
//
~xt_note_knob_button_14_A = 13;
~xt_note_knob_button_14_B = 68;
//
~xt_cc_knob_15_A = 24;
~xt_cc_knob_15_B = 51;
//
~xt_note_knob_button_15_A = 14;
~xt_note_knob_button_15_B = 69;
//
~xt_cc_knob_16_A = 25;
~xt_cc_knob_16_B = 52;
//
~xt_note_knob_button_16_A = 15;
~xt_note_knob_button_16_B = 70;

// playback cluster control buttons
//
~xt_note_rew_A = 49;
~xt_note_rew_B = 104;
//
~xt_note_ffwd_A = 50;
~xt_note_ffwd_B = 105;
//
~xt_note_loop_A = 51;
~xt_note_loop_B = 106;
//
~xt_note_rec_A = 52;
~xt_note_rec_B = 107;
//
~xt_note_stop_A = 53;
~xt_note_stop_B = 108;
//
~xt_note_play_A = 54;
~xt_note_play_B = 109;

// current midi layer status
~midi_layer = nil;

// switch midi control surface to a layer (0 -> A, 1 -> B)
~midi_set_layer = { | layer = 0 |
	// "SET LAYER to %\n".postf(layer);
	if (~midi_layer.isNil, {
		if (~midi_out.notNil, {
			~midi_out.program(~xtouch_global_ch, layer);
		});
		~midi_layer = layer;
	}, {
		if (~midi_layer != layer, {
			// "SWITCH LAYER to %\n".postf(layer);
			if (~midi_out.notNil, {
				~midi_out.program(~xtouch_global_ch, layer);
			});
			~midi_layer = layer;
		});
	});
};

// current gui layer status
~gui_layer = nil;

// switch GUI to a layer (0 -> A, 1 -> B)
~gui_set_layer = { | layer = 0 |
	if (~gui_layer.isNil, {
		~gui_layer = layer;
		~gui_active_loopers_update.(layer);
	}, {
		if (~gui_layer != layer, {
			~gui_layer = layer;
			~gui_active_loopers_update.(layer);
		});
	});
};

// convert fader value to amplitude though a value -> db table
// (modeled on the Yamaha DM1000 faders)
//
// value table (0..127, fader position)
~fader_value = [
	1, 7, 14, 20, 32, 48, 64, 79, 95, 110, 127
];
// dB table
~fader_dB = [
	-80, -50, -40, -30, -20, -15, -10, -5, 0, 5, 10
];
// initial value = -5dB
~fader_init_dB_zero = 0;
~fader_init_dB = -5;
~fader_init_dB_low = -15;

~fader_zones = ~fader_dB.size;

// convert fader value to amplitude value
~fader_value_to_amp = { | value, dB_offset = 0.0, scale_by = 1.0, value_min = 0, value_max = 127 |
	// scale any range to existing midi table
	var scaled = value.linlin(value_min, value_max, 0, 127);
	if (scaled < 1, {
		// -inf gain, return 0
		0;
	}, {
		// interpolate
		((~fader_dB + dB_offset).blendAt(~fader_value.indexInBetween(scaled)).dbamp) * scale_by;
	});
};

// convert fader dB value to fader value
~fader_dB_to_value = { | dB, dB_offset = 0.0, scale_by = 1.0, value_min = 0, value_max = 127 |
	if (dB < ~fader_dB[0], {
		0;
	}, {
		((~fader_value.blendAt((~fader_dB + dB_offset).indexInBetween(dB))).linlin(0, 127, value_min, value_max)) * scale_by;
	});
};

// convert amplitude to fader value
~fader_amp_to_value = { | amp, dB_offset = 0.0, scale_by = 1.0, value_min = 0, value_max = 127 |
	var dB = amp.ampdb;
	~fader_dB_to_value.(dB, dB_offset: dB_offset, scale_by: scale_by, value_min: value_min, value_max: value_max);
};

//
// Main surface mode, show all 8 faders or blocks of 4 dual faders
//
// \strip_all, \strip_1_4, \strip_5_8
// one for each layer
//
// later changed to a single value for both layers, makes more sense
// when operating the interface, when switching layers the mode of the
// surface remains the same (easier)

~strip_mode_state = Array.fill(2, { \strip_all });

//
// State variables
//

// loop rate (0.125 .. 1.0 .. 4.0)
~loop_rate_state = Array.fill(~loop_count, { 1.0 });
// loop rate sign (1, -1)
~loop_rate_sign_state = Array.fill(~loop_count, { 1.0 });
// loop level (raw amplitude values)
~loop_level_state = Array.fill(~loop_count, { 1.0 });
// loop balance (raw amplitude values)
~loop_balance_state = Array.fill(~loop_count, { 1.0 });
// loop spectral bend
~loop_spectral_bend_state = Array.fill(~loop_count, { 0 });
// loop overdub state (0, 1)
~loop_dub_state = Array.fill(~loop_count, { 0 });
// loop recording state (0, 1)
~loop_rec_state = Array.fill(~loop_count, { 0 });
// loop playback mute state (0, 1)
~loop_mute_state = Array.fill(~loop_count, { 1.0 });
// loop source A enable
~loop_rec_source_A_state = Array.fill(~loop_count, { 1 });
// loop source B enable
~loop_rec_source_B_state = Array.fill(~loop_count, { 1 });
// loop wait status (1 -> loop quits at end and waits for external restart)
~loop_wait_state = Array.fill(~loop_count, { 0 });
// loop sync status (1 -> loop restart waits for external sync trigger)
~loop_sync_state = Array.fill(~loop_count, { 0 });
// loop overlap state (> 1.0 -> loop restarts after overlap delay)
~loop_overlap_state = Array.fill(~loop_count, { 1.0 });

// trajectory parameters (for each layer)

// lissajous X k offset
~trajectory_kx_state = Array.fill(~layer_count, { 0 });
// lissajous Y k offset
~trajectory_ky_state = Array.fill(~layer_count, { 0 });
// lissajous size multiplier
~trajectory_size_state = Array.fill(~layer_count, { 10.0 });
// lissajous speed multiplier
~trajectory_speed_state = Array.fill(~layer_count, { 1.0 });

// reverberation (for each layer)

// loop reverb amount
~loop_reverb_amount_state = Array.fill(~layer_count, { 0.05 });
// reverb time multiplier
~layer_reverb_time_state = Array.fill(~layer_count, { 1.0 });

// inputs

// input spectral bend
~input_spectral_bend_state = Array.fill(~input_count, { 0 });
// input level (off by default)
~input_level_state = Array.fill(~input_count, { 0.0 });
// input balance (raw amplitude values)
~input_balance_state = Array.fill(~input_count, { 1.0 });
// input mute state
~input_mute_state = Array.fill(~input_count, { 1.0 });

// looper helpers

// loop overlap trigger routine
~loop_overlap_routine = Array.fill(~loop_count, { nil });

// parameters

// spectral processor constants
~spectral_max_shift_state = 24;
~spectral_wipe_scaler_state = 0.25;
~spectral_wipe_offset_state = 0.05;

// loop sync source
~loop_sync_source_state = Array.fill(~loop_count, { 0 });

//
// State updater functions
//

// find a looper index and layer from midi controller offset

~midi_offset_to_looper = { | offset |
	var looper = nil;
	var column = nil;
	var layer = nil;
	layer = (offset > 7).asInteger;
	if (~strip_mode_state[layer] == \strip_all, {
		column = 0;
		looper = offset;
	}, {
		column = offset.odd.asInteger;
		if (layer == 0, {
			if (~strip_mode_state[layer] == \strip_1_4, {
				looper = (offset / 2).floor.asInteger;
			}, {
				looper = ((offset / 2) + 4).floor.asInteger;
			});
		}, {
			if (~strip_mode_state[layer] == \strip_1_4, {
				looper = ((offset / 2) + 4).floor.asInteger;
			}, {
				looper = ((offset / 2) + 8).floor.asInteger;
			});
		});
	});
	[looper, column, layer];
};

// find a midi controller offset and layer given the strip mode and looper number

~midi_looper_to_offset = { | looper |
	var offset = nil;
	var layer = (looper > 7).asInteger;
	if (~strip_mode_state[layer] == \strip_all, {
		offset = looper % 8;
	}, {
		if (~strip_mode_state[layer] == \strip_1_4, {
			case
			{ looper.inclusivelyBetween(0, 3) } {
				offset = looper * 2;
			}
			{ looper.inclusivelyBetween(8, 11) } {
				offset = (looper * 2) - 16;
			};
		}, {
			// \strip_5_8
			case
			{ looper.inclusivelyBetween(4, 7) } {
				offset = (looper - 4) * 2;
			}
			{ looper.inclusivelyBetween(12, 15) } {
				offset = ((looper - 4) * 2) - 16;
			};
		});
	});
	[offset, layer];
};

// loop restart

// update status indicators

~loop_status_update = { | looper, value, src, set_layer = false |
	~loop_status_state[looper] = value;
	// update instrument
	//
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			if (set_layer, {
				~midi_set_layer.(layer);
			});
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B }), if (value > 0, { 127 }, { 0 }));
		});
	});
	// update gui
	if (~loop_status_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_status_gui[looper].value = if (value > 0, { 1 }, { 0 }) }.defer;
	});
};

// turn off blink indicator light

~loop_restart_off = { | looper, set_layer = false |
	Routine({
		// turn off indicator
		// update midi
		if (~midi_out.notNil, {
			var offset, layer;
			#offset, layer = ~midi_looper_to_offset.(looper);
			if (offset.notNil, {
				if (set_layer, {
					~midi_set_layer.(layer)
				});
				~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B }), 0);
			});
		});
		// update gui
		if (~loop_status_gui[looper].notNil, {
			// off
			{ ~loop_status_gui[looper].value = 0 }.defer;
		});
	}).play(AppClock);
};

// blink restart indicator light

~loop_restart_blink = { | looper |
	Routine({
		// blink!
		//
		// update state to "blink"
		~loop_status_state[looper] = 2;
		// update midi to "off"
		if (~midi_out.notNil, {
			var offset, layer;
			#offset, layer = ~midi_looper_to_offset.(looper);
			if (offset.notNil, {
				// ~midi_set_layer.(layer);
				~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B }), 0);
			});
		});
		// update gui to "red"
		if (~loop_status_gui[looper].notNil, {
			// red
			{ ~loop_status_gui[looper].value = 2 }.defer;
		});
		0.25.wait;
		// and turn indicators back on
		//
		// update state to "active" if there is something recorded in the looper
		if (~loop_dur_state[looper] > 0, {
			~loop_status_state[looper] = 1;
		});
		// update midi to "on" if there is something recorded in the looper
		if (~midi_out.notNil, {
			var offset, layer;
			#offset, layer = ~midi_looper_to_offset.(looper);
			if (offset.notNil, {
				// ~midi_set_layer.(layer);
				if (~loop_dur_state[looper] > 0, {
					~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B }), 127);
				});
			});
		});
		// update gui to "green" if there is something recorded in the looper
		if (~loop_status_gui[looper].notNil, {
			// green or stays grey
			if (~loop_dur_state[looper]  > 0 , {
				{ ~loop_status_gui[looper].value = 1 }.defer;
			});
		});
	}).play(AppClock);
};

// restart looper instrument for looper

~loop_restart = { | looper |
	var player, output;
	var order = if (0.5.coin, { 1 }, { 0 });
	if (order == 1, {
		"PLAY: 5th order\n".postf();
		player = \loop_play;
		output = 0;
	}, {
		"PLAY: 2nd order\n".postf();
		player = \loop_play_2o;
		output = 36;
		//player = \loop_play;
		//output = 0;
	});
	if (~loopers[looper].notNil, {
		// looper was already running, stop the synth
		~loopers[looper].set(\gate, 0);
		~loopers[looper] = nil;
	});
	// start loop playback
	~loopers[looper] = Synth(player, [\output, output, \reverb, ~reverb_bus[if (looper > 7, { 1 }, { 0 })].index,
		\bufindex, looper, \bufnum, ~loops[looper].bufnum, \end, ~loop_dur_state[looper],
		\master, ~master_level_state, \level, ~loop_level_state[looper], \balance, ~loop_balance_state[looper], \gate, 1,
		\mute, ~loop_mute_state[looper], \rate, ~loop_rate_state[looper], \sign, ~loop_rate_sign_state[looper],
		\vector, ~pan_control_buses[looper], \wait, ~loop_wait_state[looper],
		// spectral processing
		\mod_freq, (0.21 + 0.06.sum3rand),
		\polarity, if (0.5.coin, { 1 }, { -1 }),
		\shift, ~loop_spectral_bend_state[looper] * ~spectral_max_shift_state,
		\scram_wipe, (~loop_spectral_bend_state[looper] * ~spectral_wipe_scaler_state) + ~spectral_wipe_offset_state],
		~gen_group);
};

~loop_restart_update = { | looper, value, src |
	// for any transition of the button trigger a loop restart
	~loop_restart.(looper);
	// and update the guis...
	~loop_restart_blink.(looper);
};

// loop rate

~loop_rate_update = { | looper, value, src |
	~loop_rate_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\rate, value);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.control(~xtouch_ch, offset +  if (layer == 0, { ~xt_cc_knob_A }, { ~xt_cc_knob_B }), value.bilin(1.0, 0.125, 4.0, 64, 0, 127));
		});
	});
	// update gui
	if (~loop_rate_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_rate_gui[looper].value = value.bilin(1.0, 0.125, 4.0, 0.5, 0.0, 1.0) }.defer;
	});
};

// loop rate sign (reverse direction)

~loop_rate_sign_update = { | looper, value, src |
	~loop_rate_sign_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\sign, ~loop_rate_sign_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			// "REVERSE: update midi % on layer % to % (%)\n".postf(looper, layer, value, ~midi_layer);
			~midi_set_layer.(layer);
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_2_A }, { ~xt_note_fader_button_2_B }), if (value == 1, { 0 }, { 127 }));
		});
	});
	// update gui
	if (~loop_reverse_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_reverse_gui[looper].value = if (value == 1, { 0 }, { 1 }) }.defer;
	});
};

// loop record update

// start recording or overdubbing

~loop_record = { | looper, record, overdub |
	if (record == 1, {
		if (~loop_recorder[looper].isNil, {
			var bufi;
			if (overdub == 0, {
				bufi = ~loop_count;
				~play_phase_updated.test = true;
			}, {
				bufi = looper;
				// ask for current phase of player
				"looper %: report play phase\n".postf();
				~loopers[looper].set(\location, 1);
				~play_phase_updated.test = false;
			});
			Routine({
				var phase = 0;
				if (overdub == 1, {
					"looper %: waiting for phase...\n".postf(looper);
					~play_phase_updated.wait;
					phase = ~loop_phase_state[looper];
				});
				~loop_recorder[looper] = Synth(\loop_rec, [\bufindex, looper, \bufnum, ~loops[bufi].bufnum,
					\rec, record, \overdub, overdub,
					\source_A, ~loop_rec_source_A_state[looper], \source_B, ~loop_rec_source_B_state[looper],
					\start, phase, \end, ~loop_dur_state[looper]], ~gen_group);
				"recorder: start loop %, overdub %, bufnum %, phase %, end %\n".postf(looper, overdub, ~loops[bufi].bufnum,
					phase, ~loop_dur_state[looper]);
			}).play(SystemClock);
		}, {
			"ERROR: starting loop recorder with a recorder already running!\n".postf();
		});
	}, {
		if (~loop_recorder[looper].notNil, {
			~loop_recorder[looper].set(\rec, record);
			"recorder: stop loop %\n".postf(looper);
			~loop_recorder[looper] = nil;
		}, {
			// "ERROR: stopping loop recorder but it is not running!\n".postf();
		});
	});
};

// stop all running recorders (should be only one)

~loop_stop_recorders = {
	// stop all other recordings
	~loop_recorder.do({ | synth, active_looper |
		if (synth.notNil, {
			// found a running recorder, stop it...
			if (~loop_rec_state[active_looper] != 0, {
				// recording
				~loop_rec_update.(active_looper, 0, nil);
				"AUTO: stopped rec button for: %\n".postf(active_looper);
			}, {
				// (must be) overdubing
				~loop_dub_update.(active_looper, 0, nil);
				"AUTO: stopped dub button for: %\n".postf(active_looper);
			});
		});
	});
};

// update overdubing state

~loop_dub_update = { | looper, record, src |
	~loop_dub_state[looper] = record;
	~loop_record.(looper, ~loop_dub_state[looper], 1.0);
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_3_A }, { ~xt_note_fader_button_3_B }), record.linlin(0, 1, 0, 127));
		});
	});
	// update gui
	if (~loop_dub_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_dub_gui[looper].value = record }.defer;
	});
};

// force overdubbing to off

~loop_dub_off = { | looper |
	// update midi
	if (~midi_out.notNil, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_3_A }, { ~xt_note_fader_button_3_B }), 0);
		});
	});
	// update gui
	if (~loop_dub_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_dub_gui[looper].value = 0 }.defer;
	});
};

// update recording state

~loop_rec_update = { | looper, record, src |
	~loop_rec_state[looper] = record;
	~loop_record.(looper, ~loop_rec_state[looper], 0);
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B }), record.linlin(0, 1, 0, 127));
		});
	});
	// update gui
	if (~loop_rec_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_rec_gui[looper].value = record }.defer;
	});
};

// force recording state to off

~loop_rec_off = { | looper |
	// update midi
	if (~midi_out.notNil, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.noteOn(~xtouch_ch, offset + if (layer == 0, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B }), 0);
		});
	});
	// update gui
	if (~loop_rec_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_rec_gui[looper].value = 0 }.defer;
	});
};

// loop level
//
// input value: amplitude from 0 to max

~loop_level_update = { | looper, value, src |
	~loop_level_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\level, ~loop_level_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (offset.notNil, {
			~midi_set_layer.(layer);
			~midi_out.control(~xtouch_ch, offset + if (layer == 0, { ~xt_cc_fader_A }, { ~xt_cc_fader_B }), ~fader_amp_to_value.(value, value_min: 0, value_max: 127));
		});
	});
	// update gui
	if (~loop_level_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_level_gui[looper].value = ~fader_amp_to_value.(value, value_min: 0.0, value_max: 1.0) }.defer;
	});
};

// loop spectral processor balance

~loop_balance_update = { | looper, value, src |
	~loop_balance_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\balance, ~loop_balance_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.control(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_cc_fader_A }, { ~xt_cc_fader_B }),
					~fader_amp_to_value.(value, dB_offset: -10, value_min: 0, value_max: 127));
			});
		});
	});
	// update gui
	if (~loop_balance_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_balance_gui[looper].value = ~fader_amp_to_value.(value, dB_offset: -10, value_min: 0.0, value_max: 1.0) }.defer;
	});
};

// loop spectral bend amount

~loop_spectral_bend_update = { | looper, value, src |
	// update state
	~loop_spectral_bend_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\shift, value * ~spectral_max_shift_state);
		~loopers[looper].set(\scram_wipe, (value * ~spectral_wipe_scaler_state) + ~spectral_wipe_offset_state);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.control(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_cc_knob_A }, { ~xt_cc_knob_B }), value.bilin(0, -1, 1, 64, 0, 127));
			});
		});
	});
	// update gui (no gui yet)
	if (~loop_spectral_bend_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_spectral_bend_gui[looper].value = value.bilin(0, -1, 1, 0.5, 0, 1) }.defer;
	});
};

// loop sync status, 0 -> free running, 1 -> sync to clock

~loop_sync_update = { | looper, value, src |
	// update state
	~loop_sync_state[looper] = value;
	if (~loop_overlap_state == 1, {
		~loop_wait_state[looper] = value;
	});
	// update instrument
	if (~loopers[looper].notNil, {
		if (~loop_overlap_state == 1, {
			~loopers[looper].set(\wait, if (value == 0, { 0 }, { 1 }));
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.noteOn(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B }), if (value == 0, { 0 }, { 127 }));
			});
		});
	});
	// update gui
	if (~loop_sync_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_sync_gui[looper].value = ~loop_sync_state[looper] }.defer;
	});
};

// enable source A for looper

~loop_rec_source_A_update = { | looper, value, src |
	// update state
	~loop_rec_source_A_state[looper] = value;
	// update instrument
	if (~loop_recorder[looper].notNil, {
		~loop_recorder[looper].set(\source_A, ~loop_rec_source_A_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.noteOn(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_note_fader_button_2_A }, { ~xt_note_fader_button_2_B }), value.linlin(0, 1, 0, 127));
			});
		});
	});
	// update gui
	if (~loop_rec_source_A_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_rec_source_A_gui[looper].value = ~loop_rec_source_A_state[looper] }.defer;
	});
};

// enable source B for looper

~loop_rec_source_B_update = { | looper, value, src |
	// update state
	~loop_rec_source_B_state[looper] = value;
	// update instrument
	if (~loop_recorder[looper].notNil, {
		~loop_recorder[looper].set(\source_B, ~loop_rec_source_A_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.noteOn(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_note_fader_button_3_A }, { ~xt_note_fader_button_3_B }), value.linlin(0, 1, 0, 127));
			});
		});
	});
	// update gui
	if (~loop_rec_source_B_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_rec_source_B_gui[looper].value = ~loop_rec_source_B_state[looper] }.defer;
	});
};

// loop playback mute

~loop_mute_update = { | looper, value, src |
	~loop_mute_state[looper] = value;
	// update instrument
	if (~loopers[looper].notNil, {
		~loopers[looper].set(\mute, ~loop_mute_state[looper]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var offset, layer;
		#offset, layer = ~midi_looper_to_offset.(looper);
		if (~strip_mode_state[layer] != \strip_all, {
			if (offset.notNil, {
				~midi_set_layer.(layer);
				~midi_out.noteOn(~xtouch_ch, offset + 1 + if (layer == 0, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B }), if (value == 0, { 127 }, { 0 }));
			});
		});
	});
	// update gui
	if (~loop_mute_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_mute_gui[looper].value = 1 - value; }.defer;
	});
};

//
// shared loop controls
//

// trajectory k oscillator offset for X

~trajectory_kx_update = { | layer, value, src |
	var panners;
	// update state
	~trajectory_kx_state[layer] = value;
	// update instruments
	if (layer == 0, {
		panners = (0 .. 7) ++ 16;
	}, {
		panners = (8 .. 15) ++ 17;
	});
	panners.do ({ | panner |
		if (~pan_controllers[panner].notNil, {
			~pan_controllers[panner].set(\kx, value);
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_9_A }, { ~xt_cc_knob_9_B }), value.linlin(0, 3, 0, 127));
	});
	// update gui (no gui yet)
};

// trajectory k oscillator offset for Y

~trajectory_ky_update = { | layer, value, src |
	var panners;
	// update state
	~trajectory_ky_state[layer] = value;
	// update instruments
	if (layer == 0, {
		panners = (0 .. 7) ++ 16;
	}, {
		panners = (8 .. 15) ++ 17;
	});
	panners.do ({ | panner |
		if (~pan_controllers[panner].notNil, {
			~pan_controllers[panner].set(\ky, value);
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_10_A }, { ~xt_cc_knob_10_B }), value.linlin(0, 3, 0, 127));
	});
	// update gui (no gui yet)
};

// trajectory size multiplier

~trajectory_size_update = { | layer, value, src |
	var panners;
	// update state
	~trajectory_size_state[layer] = value;
	// update instruments
	if (layer == 0, {
		panners = (0 .. 7) ++ 16;
	}, {
		panners = (8 .. 15) ++ 17;
	});
	panners.do ({ | panner |
		if (~pan_controllers[panner].notNil, {
			~pan_controllers[panner].set(\size, value);
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_11_A }, { ~xt_cc_knob_11_B }), value.bilin(10, 4, 30, 64, 0, 127));
	});
	// update gui (no gui yet)
};

// trajectory speed multiplier

~trajectory_speed_update = { | layer, value, src |
	var panners;
	// update state
	~trajectory_speed_state[layer] = value;
	// update instruments
	if (layer == 0, {
		panners = (0 .. 7) ++ 16;
	}, {
		panners = (8 .. 15) ++ 17;
	});
	panners.do ({ | panner |
		if (~pan_controllers[panner].notNil, {
			~pan_controllers[panner].set(\speed, value);
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_12_A }, { ~xt_cc_knob_12_B }), value.linlin(0, 10, 0, 127));
	});
	// update gui (no gui yet)
	//
	"SPEED: updated to % for layer %\n".postf(value, value, layer)
};

// loop reverb amount

~loop_reverb_amount_update = { | layer, value, src |
	var loopers;
	// update state
	~loop_reverb_amount_state[layer] = value;
	// update instruments
	if (layer == 0, {
		loopers = (0 .. 7) ++ 16;
	}, {
		loopers = (8 .. 15) ++ 17;
	});
	loopers.do ({ | looper |
		if (~loopers[looper].notNil, {
			~loopers[looper].set(\reverb_amount, value);
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_13_A }, { ~xt_cc_knob_13_B }), value.linlin(0, 1, 0, 127));
	});
	// update gui (no gui yet)
};

// layer reverb time multiplier

~layer_reverb_time_update = { | layer, value, src |
	// update state
	~layer_reverb_time_state[layer] = value;
	// update instruments
	if (~reverb[layer].notNil, {
		~reverb[layer].set(\rt_mult, value);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_14_A }, { ~xt_cc_knob_14_B }), value.bilin(1.0, 0.25, 3, 64, 0, 127));
	});
	// update gui (no gui yet)
};

// layer loop overlap multiplier

~layer_overlap_update = { | layer, value, src |
	// update state
	var loopers;
	if (layer == 0, {
		loopers = (0 .. 7);
	}, {
		loopers = (8 .. 15);
	});
	loopers.do ({ | looper | ~loop_overlap_state[looper] = value; });
	// update instruments
	if (value > 1.0, {
		// enable overlap processing
		loopers.do ({ | looper |
			~loop_wait_state[looper] = 1;
			if (~loopers[looper].notNil, {
				~loopers[looper].set(\wait, 1);
			});
		});
	}, {
		// disable overlap processing
		loopers.do ({ | looper |
			if (~loop_sync_state[looper] == 0, {
				// only disable wait if sync is not active
				~loop_wait_state[looper] = 0;
				if (~loopers[looper].notNil, {
					~loopers[looper].set(\wait, 0);
				});
			});
		});
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_10_A }, { ~xt_cc_knob_10_B }), value.linlin(1.0, 4.0, 0, 127));
	});
	// update gui (no gui yet)
};

//
// inputs
//

// input spectral bend amount

~input_spectral_bend_update = { | input, value, src |
	// update state
	~input_spectral_bend_state[input] = value;
	// update instrument
	if (~inputs[input].notNil, {
		~inputs[input].set(\shift, value * ~spectral_max_shift_state);
		~inputs[input].set(\scram_wipe, (value * ~spectral_wipe_scaler_state) + ~spectral_wipe_offset_state);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var layer = input;
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_16_A }, { ~xt_cc_knob_16_B }), value.bilin(0, -1, 1, 64, 0, 127));
	});
	// update gui
	if (~input_spectral_bend_gui[input].notNil, {
		~gui_set_layer.(input);
		{ ~input_spectral_bend_gui[input].value = value.bilin(0, -1, 1, 0.5, 0, 1) }.defer;
	});
};

// input level

~input_level_update = { | input, value, src |
	~input_level_state[input] = value;
	// update instrument
	if (~inputs[input].notNil, {
		~inputs[input].set(\level, ~input_level_state[input]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var layer = input;
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_fader_9_A }, { ~xt_cc_fader_9_B }), ~fader_amp_to_value.(value, value_min: 0, value_max: 127));
	});
	// update gui
	if (~input_level_gui[input].notNil, {
		~gui_set_layer.(input);
		{ ~input_level_gui[input].value = ~fader_amp_to_value.(value, value_min: 0.0, value_max: 1.0) }.defer;
	});
};

// input spectral processor balance

~input_balance_update = { | input, value, src |
	~input_balance_state[input] = value;
	// update instrument
	if (~inputs[input].notNil, {
		~inputs[input].set(\balance, ~input_balance_state[input]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var layer = input;
		~midi_set_layer.(layer);
		~midi_out.control(~xtouch_ch, if (layer == 0, { ~xt_cc_knob_15_A }, { ~xt_cc_knob_15_B }),
			~fader_amp_to_value.(value, dB_offset: -10, value_min: 0, value_max: 127));
	});
	// update gui
	if (~input_balance_gui[input].notNil, {
		~gui_set_layer.(input);
		{ ~input_balance_gui[input].value = ~fader_amp_to_value.(value, dB_offset: -10, value_min: 0.0, value_max: 1.0) }.defer;
	});
};

// input playback mute

~input_mute_update = { | input, value, src |
	~input_mute_state[input] = value;
	// update instrument
	if (~inputs[input].notNil, {
		~inputs[input].set(\mute, ~input_mute_state[input]);
	});
	// update midi
	if (~midi_out.notNil and: { src != \midi }, {
		var layer = input;
		~midi_set_layer.(layer);
		~midi_out.noteOn(~xtouch_ch, 8 + if (layer == 0, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B }), if (value == 0, { 127 }, { 0 }));
	});
	// update gui
	if (~input_mute_gui[input].notNil, {
		~gui_set_layer.(input);
		{ ~input_mute_gui[input].value = 1 - value; }.defer;
	});
};

// update looper source sync

~loop_sync_source_update = { | looper, value, src |
	~loop_sync_source_state[looper] = value;
	// update instrument (nothing to be done)
	//
	// update midi (no midi yet)
	//
	// update gui
	if (~loop_sync_source_gui[looper].notNil, {
		~gui_set_layer.(if (looper > 7, { 1 }, { 0 }));
		{ ~loop_sync_source_gui[looper].value = value; }.defer;
	});
};

// update mixer strip background colors

~gui_looper_background_update = { | layer, looper, on |
	var inactive = Color.grey(0.7);
	var color = if (on, { Color.grey(0.3) }, { Color.grey(0.7) });
	var k_inactive = Color.grey(0.9);
	var k_color = if (on, { Color.grey(0.5) }, { Color.grey(0.9) });
	if (~loop_rate_gui[looper].notNil, {
		~loop_rate_gui[looper].color = [k_color, Color.black, k_color, Color.black];
	});
	if (~loop_level_gui[looper].notNil, {
		~loop_level_gui[looper].setColors(Color.grey, Color.white, color, Color.grey, Color.white, Color.yellow, nil, Color.blue(0.5), Color.grey(0.7));
	});
	if (~loop_balance_gui[looper].notNil, {
		if (~strip_mode_state[layer] != \strip_all, {
			~loop_balance_gui[looper].setColors(Color.grey, Color.white, color, Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		}, {
			~loop_balance_gui[looper].setColors(Color.grey, Color.white, inactive, Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		});
	});
	if (~loop_spectral_bend_gui[looper].notNil, {
		if (~strip_mode_state[layer] != \strip_all, {
			~loop_spectral_bend_gui[looper].color = [k_color, Color.black, k_color, Color.black];
		}, {
			~loop_spectral_bend_gui[looper].color = [k_inactive, Color.black, k_inactive, Color.black];
		});
	});
};

// update backgroup colors in all mixer strips

~gui_active_loopers_update = { | layer |
	// update gui backgrounds to reflect active layer and strip mode
	var loopers = nil;
	// determine offset in gui
	if (~strip_mode_state[layer] == \strip_all, {
		if (layer == 0, {
			loopers = (0 .. 7);
		}, {
			loopers = (8 .. 15);
		});
	});
	if (~strip_mode_state[layer] == \strip_1_4, {
		if (layer == 0, {
			loopers = (0 .. 3);
		}, {
			loopers = (8 .. 11);
		});
	});
	if (~strip_mode_state[layer] == \strip_5_8, {
		if (layer == 0, {
			loopers = (4 .. 7);
		}, {
			loopers = (12 .. 15);
		});
	});
	if (~gui_active_loopers.notNil, {
		{
			~loopers.do ({ | looper, index |
				if (loopers.includes(index), {
					// midi is active for this looper, highlight it in gui
					~gui_looper_background_update.(layer, index, true);
				}, {
					// looper not in midi surface at this point
					~gui_looper_background_update.(layer, index, false);
				});
			});
		}.defer;
	});
};

// update strip mode and layer (\strip_all, \strip_1_4 or \strip_5_8)

~update_surface = { | mode, layer |
	var loopers = nil;
	var gui_loopers = nil;
	if (mode == \strip_all, {
		if (layer == 0, {
			loopers = (0 .. 7);
		}, {
			loopers = (8 .. 15);
		});
		gui_loopers = (0 .. 7);
	});
	if (mode == \strip_1_4, {
		if (layer == 0, {
			loopers = (0 .. 3);
		}, {
			loopers = (8 .. 11);
		});
		gui_loopers = (0 .. 3);
	});
	if (mode == \strip_5_8, {
		if (layer == 0, {
			loopers = (4 .. 7);
		}, {
			loopers = (12 .. 15);
		});
		gui_loopers = (4 .. 7);
	});
	~gui_active_loopers = gui_loopers;
	// "UPDATE: loopers % to mode % (layer %)\n".postf(loopers, mode, layer);
	// update midi control surface to reflect change
	loopers.do ({ | looper |
		// loop rate knob
		~loop_rate_update.(looper, ~loop_rate_state[looper], nil);
		// loop spectral bend knob
		~loop_spectral_bend_update.(looper, ~loop_spectral_bend_state[looper], nil);
		// loop status indicator
		~loop_status_update.(looper, ~loop_status_state[looper], nil);
		// loop reverse button
		~loop_rate_sign_update.(looper, ~loop_rate_sign_state[looper], nil);
		// loop overdub button
		~loop_dub_update.(looper, ~loop_dub_state[looper], nil);
		// loop level fader
		~loop_level_update.(looper, ~loop_level_state[looper], nil);
		// loop balance fader
		~loop_balance_update.(looper, ~loop_balance_state[looper], nil);
		// loop record button
		~loop_rec_update.(looper, ~loop_rec_state[looper], nil);
		// loop playback mute button
		~loop_mute_update.(looper, ~loop_mute_state[looper], nil);
		// loop playback sync button
		~loop_sync_update.(looper, ~loop_sync_state[looper], nil);
		// loop source A
		~loop_rec_source_A_update.(looper, ~loop_rec_source_A_state[looper], nil);
		// loop sourcec B
		~loop_rec_source_B_update.(looper, ~loop_rec_source_B_state[looper], nil);
	});
};

~strip_mode_update = { | mode, layer |
	// update state
	var prev_mode = ~strip_mode_state[layer];
	var order = nil;
	// "LAYER: mode = %, layer = %\n".postf(mode, layer);
	// define update order (we have to update both layers in the right order)
	if (~midi_layer.isNil, {
		// layer is current layer the first time through
		~midi_layer = layer;
	});
	if (layer != ~midi_layer, {
		// change layer, first old then new
		order = [~midi_layer, layer];
		// "LAYER: change from % to %\n".postf(~midi_layer, layer);
	}, {
		// stay in layer... which one?
		if (layer == 0, {
			order = [1, 0];
		}, {
			order = [0, 1];
		});
		// "LAYER: stay in %\n".postf(layer);
	});
	// update both layers at the same time, the mode is invariant to the layer selected
	// and bring surface up to date
	order.do ({ | layer |
		// "SURFACE: layer % to mode % (old %)\n".postf(layer, mode, ~midi_layer);
		~strip_mode_state[layer] = mode;
		// force a layer switch
		//
		// FIXME: why is this needed? still have to debug this...
		~midi_layer = 1 - layer;
		~midi_set_layer.(layer);
		~update_surface.(mode, layer);
	});
	// update midi
	if (~midi_out.notNil, {
		order.do ({ | layer |
			~midi_set_layer.(layer);
			if (mode == \strip_all, {
				// turn off both buttons
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_stop_A }, { ~xt_note_stop_B }), 0);
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_play_A }, { ~xt_note_play_B }), 0);
			});
			if (mode == \strip_1_4, {
				// turn on 1 to 4, off 5 to 8
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_stop_A }, { ~xt_note_stop_B }), 127);
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_play_A }, { ~xt_note_play_B }), 0);
			});
			if (mode == \strip_5_8, {
				// turn on 5 to 8, off 1 to 4
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_stop_A }, { ~xt_note_stop_B }), 0);
				~midi_out.noteOn(~xtouch_ch, if (layer == 0, { ~xt_note_play_A }, { ~xt_note_play_B }), 127);
			});
		});
	});
	// update gui (no gui yet)
};

//
// MIDI control
//

// turn off button in control surface

~midi_button_off = { | control |
	if (~midi_out.notNil, {
		~midi_out.noteOn(~xtouch_ch, control, 0);
	});
};

// loop rate or spectral bend knobs
~loop_count.do ({ | offset |
	var cc = (offset % 8) + if (offset < 8, { ~xt_cc_knob_A }, { ~xt_cc_knob_B });
	MIDIdef.cc(\loop_rate_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_rate_update.(looper, value.bilin(64, 0, 127, 1.0, 0.125, 4.0), \midi);
		}, {
			~loop_spectral_bend_update.(looper, value.bilin(64, 0, 127, 0, -1, 1), \midi);
		});
	}, cc, ~xtouch_ch);
});

// loop rate center buttons
~loop_count.do ({ | offset |
	var note = (offset % 8) + if (offset < 8, { ~xt_note_knob_button_A }, { ~xt_note_knob_button_B });
	MIDIdef.noteOn(\loop_rate_center_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			// "nil" source so that midi surface rate is updated even when coming from here (\midi)
			~loop_rate_update.(looper, 64.bilin(64, 0, 127, 1.0, 0.125, 4.0), nil);
		}, {
			// not used, turn off
			~midi_button_off.(note);
		});
	}, note, ~xtouch_ch);
});

// loop restart buttons
// ON
~loop_count.do ({ | offset |
	var note = (offset % 8 ) + if (offset < 8, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B });
	MIDIdef.noteOn(\loop_restart_on_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			if (~loop_dur_state[looper] > 0, {
				// only restart if something has been recorded
				~loop_restart_update.(looper, value.linlin(0, 127, 0, 1), \midi);
				"MIDI: loop restart (on) %: %\n".postf(looper, value);
			}, {
				~loop_restart_off.(looper);
			});
		}, {
			if (~loop_sync_source_state[looper] != 0, {
				// we cannot set source and destination of sync to the same looper
				value = 0;
			});
			~loop_sync_update.(looper, 1, \midi);
			"MIDI: sync ON: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// OFF
~loop_count.do ({ | offset |
	var note = (offset % 8 ) + if (offset < 8, { ~xt_note_fader_button_1_A }, { ~xt_note_fader_button_1_B });
	MIDIdef.noteOff(\loop_restart_off_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			if (~loop_dur_state[looper] > 0, {
				~loop_restart_update.(looper, value.linlin(0, 127, 0, 1), \midi);
				"MIDI: loop restart (off) %: %\n".postf(looper, value);
			}, {
				~loop_restart_off.(looper);
			});
		}, {
			~loop_sync_update.(looper, 0, \midi);
			"MIDI: sync OFF: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// loop reverse or source A buttons
// ON
~loop_count.do ({ | offset |
	var note = (offset % 8) + if (offset < 8, { ~xt_note_fader_button_2_A }, { ~xt_note_fader_button_2_B });
	MIDIdef.noteOn(\loop_rate_sign_on_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_rate_sign_update.(looper, -1, \midi);
			"MIDI: reverse button ON: % % %\n".postf(value, num, chan);
		}, {
			~loop_rec_source_A_update.(looper, 1, \midi);
			"MIDI: source A ON: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// OFF
~loop_count.do ({ | offset |
	var note = (offset % 8) + if (offset < 8, { ~xt_note_fader_button_2_A }, { ~xt_note_fader_button_2_B });
	MIDIdef.noteOff(\loop_rate_sign_off_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_rate_sign_update.(looper, 1, \midi);
			"MIDI: reverse button OFF: % % %\n".postf(value, num, chan);
		}, {
			~loop_rec_source_A_update.(looper, 0, \midi);
			"MIDI: source A OFF: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// loop overdub or source B buttons
// ON
~loop_count.do ({ | offset |
	var note = (offset % 8) + if (offset < 8, { ~xt_note_fader_button_3_A }, { ~xt_note_fader_button_3_B });
	MIDIdef.noteOn(\loop_dub_on_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			// stop all other recordings
			~loop_stop_recorders.();
			// and start a new one
			~loop_dub_update.(looper, 1, \midi);
			"MIDI: dub button: % % %\n".postf(value, num, chan);
		}, {
			~loop_rec_source_B_update.(looper, 1, \midi);
			"MIDI: source B ON: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// OFF
~loop_count.do ({ | offset |
	var note = (offset % 8) + if (offset < 8, { ~xt_note_fader_button_3_A }, { ~xt_note_fader_button_3_B });
	MIDIdef.noteOff(\loop_dub_off_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_dub_update.(looper, 0, \midi);
			"MIDI: dub button: % % %\n".postf(value, num, chan);
		}, {
			~loop_rec_source_B_update.(looper, 0, \midi);
			"MIDI: source B OFF: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// loop record or mute buttons
// ON
~loop_count.do ({ | offset |
	var note = (offset % 8 ) + if (offset < 8, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B });
	MIDIdef.noteOn(\loop_rec_on_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			// stop all other recordings
			~loop_stop_recorders.();
			// and start a new one
			~loop_rec_update.(looper, 1, \midi);
			"MIDI: rec button: % % %\n".postf(value, num, chan);
		}, {
			~loop_mute_update.(looper, 0, \midi);
			"MIDI: mute ON: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// OFF
~loop_count.do ({ | offset |
	var note = (offset % 8 ) + if (offset < 8, { ~xt_note_fader_button_4_A }, { ~xt_note_fader_button_4_B });
	MIDIdef.noteOff(\loop_rec_off_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_rec_update.(looper, 0, \midi);
			"MIDI: rec button: % % %\n".postf(value, num, chan);
		}, {
			~loop_mute_update.(looper, 1, \midi);
			"MIDI: mute OFF: % % %\n".postf(value, num, chan);
		});
	}, note, ~xtouch_ch);
});

// loop level or balance faders
~loop_count.do ({ | offset |
	var cc = (offset % 8) + if (offset < 8, { ~xt_cc_fader_A }, { ~xt_cc_fader_B });
	"define % fader responder for %\n".postf(offset, cc);
	MIDIdef.cc(\loop_level_++(offset.asString), { | value, num, chan |
		var looper, column, layer;
		#looper, column, layer = ~midi_offset_to_looper.(offset);
		if (column == 0, {
			~loop_level_update.(looper, ~fader_value_to_amp.(value, value_min: 0, value_max: 127), \midi);
			// "MIDI: % fader %: %\n".postf(offset, cc, value);
		}, {
			~loop_balance_update.(looper, ~fader_value_to_amp.(value, dB_offset: -10, value_min: 0, value_max: 127), \midi);
			// "MIDI: % balance %: %\n".postf(offset, cc, value);
		});
	}, cc, ~xtouch_ch);
});

//
// shared control knobs
//

// shared trajectory controls

// trajectory X k offset
//
// layer A
MIDIdef.cc(\trajectory_kx_A, { | value, num, chan |
	~trajectory_kx_update.(0, value.linlin(0, 127, 0, 3), \midi);
}, ~xt_cc_knob_9_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\trajectory_kx_B, { | value, num, chan |
	~trajectory_kx_update.(1, value.linlin(0, 127, 0, 3), \midi);
}, ~xt_cc_knob_9_B, ~xtouch_ch);

// reset to default (0)
//
// layer A
MIDIdef.noteOn(\trajectory_kx_reset_A, { | value, num, chan |
	~trajectory_kx_update.(0, 0.linlin(0, 127, 0, 3), \midi);
}, ~xt_note_knob_button_9_A, ~xtouch_ch);

// layer B
MIDIdef.noteOff(\trajectory_kx_reset_B, { | value, num, chan |
	~trajectory_kx_update.(1, 0.linlin(0, 127, 0, 3), \midi);
}, ~xt_note_knob_button_9_B, ~xtouch_ch);

/*
// trajectory Y k offset
//
// layer A
MIDIdef.cc(\trajectory_ky_A, { | value, num, chan |
	~trajectory_ky_update.(0, value.linlin(0, 127, 0, 3), \midi);
}, ~xt_cc_knob_10_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\trajectory_ky_B, { | value, num, chan |
	~trajectory_ky_update.(1, value.linlin(0, 127, 0, 3), \midi);
}, ~xt_cc_knob_10_B, ~xtouch_ch);
	*/

// loop overlap amount
//
// layer A
MIDIdef.cc(\loop_overlap_A, { | value, num, chan |
	~layer_overlap_update.(0, value.linlin(0, 127, 1.0, 4.0), \midi);
}, ~xt_cc_knob_10_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\loop_overlap_B, { | value, num, chan |
	~layer_overlap_update.(1, value.linlin(0, 127, 1.0, 4.0), \midi);
}, ~xt_cc_knob_10_B, ~xtouch_ch);

// trajectory size multiplier
//
// layer A
MIDIdef.cc(\trajectory_size_A, { | value, num, chan |
	~trajectory_size_update.(0, value.bilin(64, 0, 127, 10, 4, 30), \midi);
}, ~xt_cc_knob_11_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\trajectory_size_B, { | value, num, chan |
	~trajectory_size_update.(1, value.bilin(64, 0, 127, 10, 4, 30), \midi);
}, ~xt_cc_knob_11_B, ~xtouch_ch);

// trajectory speed multiplier
//
// layer A
MIDIdef.cc(\trajectory_speed_A, { | value, num, chan |
	~trajectory_speed_update.(0, value.linlin(0, 127, 0, 10), \midi);
}, ~xt_cc_knob_12_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\trajectory_speed_B, { | value, num, chan |
	~trajectory_speed_update.(1, value.linlin(0, 127, 0, 10), \midi);
}, ~xt_cc_knob_12_B, ~xtouch_ch);

// loop reverberation amount
//
// layer A
MIDIdef.cc(\loop_reverb_amount_A, { | value, num, chan |
	~loop_reverb_amount_update.(0, value.linlin(0, 127, 0, 1), \midi);
}, ~xt_cc_knob_13_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\loop_reverb_amount_B, { | value, num, chan |
	~loop_reverb_amount_update.(1, value.linlin(0, 127, 0, 1), \midi);
}, ~xt_cc_knob_13_B, ~xtouch_ch);

// loop reverberation time multiplier
//
// layer A
MIDIdef.cc(\loop_reverb_time_A, { | value, num, chan |
	~layer_reverb_time_update.(0, value.bilin(64, 0, 127, 1.0, 0.25, 3.0), \midi);
}, ~xt_cc_knob_14_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\loop_reverb_time_B, { | value, num, chan |
	~layer_reverb_time_update.(1, value.bilin(64, 0, 127, 1.0, 0.25, 3.0), \midi);
}, ~xt_cc_knob_14_B, ~xtouch_ch);

// share input controls

// input level fader
//
// layer A
MIDIdef.cc(\input_level_A, { | value, num, chan |
	~input_level_update.(0, ~fader_value_to_amp.(value, value_min: 0, value_max: 127), \midi);
}, ~xt_cc_fader_9_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\input_level_B, { | value, num, chan |
	~input_level_update.(1, ~fader_value_to_amp.(value, value_min: 0, value_max: 127), \midi);
}, ~xt_cc_fader_9_B, ~xtouch_ch);

// input spectral processing balance knob
//
// layer A
MIDIdef.cc(\input_balance_A, { | value, num, chan |
	~input_balance_update.(0, ~fader_value_to_amp.(value, dB_offset: -10, value_min: 0, value_max: 127), \midi);
}, ~xt_cc_knob_15_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\input_balance_B, { | value, num, chan |
	~input_balance_update.(1, ~fader_value_to_amp.(value, dB_offset: -10, value_min: 0, value_max: 127), \midi);
}, ~xt_cc_knob_15_B, ~xtouch_ch);

// input spectral processor bend knob
//
// layer A
MIDIdef.cc(\input_spectral_bend_A, { | value, num, chan |
	~input_spectral_bend_update.(0, value.bilin(64, 0, 127, 0, -1, 1), \midi);
}, ~xt_cc_knob_16_A, ~xtouch_ch);

// layer B
MIDIdef.cc(\input_spectral_bend_B, { | value, num, chan |
	~input_spectral_bend_update.(1, value.bilin(64, 0, 127, 0, -1, 1), \midi);
}, ~xt_cc_knob_16_B, ~xtouch_ch);

// input mute
//
// layer A
// ON
MIDIdef.noteOn(\input_mute_A_on, { | value, num, chan |
	~input_mute_update.(0, 0, \midi);
}, ~xt_note_fader_button_4_A + 8, ~xtouch_ch);

// OFF
MIDIdef.noteOff(\input_mute_A_off, { | value, num, chan |
	~input_mute_update.(0, 1, \midi);
}, ~xt_note_fader_button_4_A + 8, ~xtouch_ch);

// layer B
// ON
MIDIdef.noteOn(\input_mute_B_on, { | value, num, chan |
	~input_mute_update.(1, 0, \midi);
}, ~xt_note_fader_button_4_B + 8, ~xtouch_ch);

// OFF
MIDIdef.noteOff(\input_mute_B_off, { | value, num, chan |
	~input_mute_update.(1, 1, \midi);
}, ~xt_note_fader_button_4_B + 8, ~xtouch_ch);

//
// shared control buttons
//

// strip mode 1 to 4 button, layer A
// ON: strips 1 to 4
MIDIdef.noteOn(\strip_mode_1_4_A, { | value, num, chan |
	// switch to 1 to 4
	~strip_mode_update.(\strip_1_4, 0);
	"MIDI: layer A strip mode 1-4 % % %\n".postf(value, num, chan);
}, ~xt_note_stop_A, ~xtouch_ch);

// OFF: all strips
MIDIdef.noteOff(\strip_mode_1_4_all_A, { | value, num, chan |
	~strip_mode_update.(\strip_all, 0);
	"MIDI: layer A strip mode all % % %\n".postf(value, num, chan);
}, ~xt_note_stop_A, ~xtouch_ch);

// strip mode 5 to 8 button, layer A
// ON: strips 5 to 8
MIDIdef.noteOn(\strip_mode_5_8_A, { | value, num, chan |
	// switch to 5 to 8
	~strip_mode_update.(\strip_5_8, 0);
	"MIDI: layer A strip mode 5-8 % % %\n".postf(value, num, chan);
}, ~xt_note_play_A, ~xtouch_ch);

// OFF: all strips
MIDIdef.noteOff(\strip_mode_5_8_all_A, { | value, num, chan |
	~strip_mode_update.(\strip_all, 0);
	"MIDI: layer A strip mode all % % %\n".postf(value, num, chan);
}, ~xt_note_play_A, ~xtouch_ch);

//
// strip mode button, layer B
// ON: strips 1 to 4
MIDIdef.noteOn(\strip_mode_1_4_B, { | value, num, chan |
	~strip_mode_update.(\strip_1_4, 1);
	"MIDI: layer B strip mode 1-4 % % %\n".postf(value, num, chan);
}, ~xt_note_stop_B, ~xtouch_ch);

// OFF: all strips
MIDIdef.noteOff(\strip_mode_1_4_all_B, { | value, num, chan |
	~strip_mode_update.(\strip_all, 1);
	"MIDI: layer B all strips % % %\n".postf(value, num, chan);
}, ~xt_note_stop_B, ~xtouch_ch);

// strip mode 5 to 8 button, layer B
// ON: strips 5 to 8
MIDIdef.noteOn(\strip_mode_5_8_B, { | value, num, chan |
	// switch to 5 to 8
	~strip_mode_update.(\strip_5_8, 1);
	"MIDI: layer B strip mode 5-8 % % %\n".postf(value, num, chan);
}, ~xt_note_play_B, ~xtouch_ch);

// OFF: all strips
MIDIdef.noteOff(\strip_mode_5_8_all_B, { | value, num, chan |
	~strip_mode_update.(\strip_all, 1);
	"MIDI: layer B strip mode all % % %\n".postf(value, num, chan);
}, ~xt_note_play_B, ~xtouch_ch);

//
// SuperCollider control GUI
//

// GUI elements

// elapsed time view
~time_view = nil;

// start elapsed timer
~start_timer_gui = nil;

// loop trajectory view
~loop_trajectory_gui = Array.fill(~loop_count, { nil });

// loop rate knob
~loop_rate_gui = Array.fill(~loop_count, { nil });

// loop spectral bend knob
~loop_spectral_bend_gui = Array.fill(~loop_count, { nil });

// loop rate center button
~loop_rate_center_gui = Array.fill(~loop_count, { nil });

// loop status and restart button
~loop_status_gui = Array.fill(~loop_count, { nil });

// loop sync button
~loop_sync_gui = Array.fill(~loop_count, { nil });

// loop reverse button
~loop_reverse_gui = Array.fill(~loop_count, { nil });

// loop source A enable button
~loop_rec_source_A_gui = Array.fill(~loop_count, { nil });

// loop overdub button
~loop_dub_gui = Array.fill(~loop_count, { nil });

// loop source B enable button
~loop_rec_source_B_gui = Array.fill(~loop_count, { nil });

// loop level fader
~loop_level_gui = Array.fill(~loop_count, { nil });

// loop balance fader
~loop_balance_gui = Array.fill(~loop_count, { nil });

// loop output level meter
~loop_meter_gui = Array.fill(~loop_count, { nil });

// loop playback location display
~loop_location_gui = Array.fill(~loop_count, { nil });

// loop record button
~loop_rec_gui = Array.fill(~loop_count, { nil });

// loop mute button
~loop_mute_gui = Array.fill(~loop_count, { nil });

//
// sync interface
//

// loop sync source
~loop_sync_source_gui = Array.fill(~loop_count, { nil });

//
// inputs
//

// input spectral bend knob
~input_spectral_bend_gui = Array.fill(~input_count, { nil });

// input tranjectory view
~input_trajectory_gui = Array.fill(~input_count, { nil });

// input level fader
~input_level_gui = Array.fill(~input_count, { nil });

// input balance fader
~input_balance_gui = Array.fill(~input_count, { nil });

// input meter
~input_meter_gui = Array.fill(~input_count, { nil });

// input mute button
~input_mute_gui = Array.fill(~input_count, { nil });


// create the GUI
~create_gui = {
	var sooperlooperHeight = 340;
	var gui_button_size = 40;
	var gui_strip_width = 45;
	var gui_fader_len = 400;
	
	var sooperlooperRect = Rect((Window.screenBounds.width/2)-200, (Window.screenBounds.height/2)-400, 20, sooperlooperHeight);
	var sooperlooper = Window.new("SooperLooper", sooperlooperRect);

	// stack vertically
	var vmaster = VLayout.new();

	// top horizontal layout
	var htop = HLayout.new();

	// for adding looper strips
	var hlay = HLayout.new();

	// for adding bottom controls
	var blay = HLayout.new();

	sooperlooper.front;

	// elapsed time counter (since the start of the piece)
	~time_view = GUI.textView.new(w, 600 @ 60);
	~time_view.hasHorizontalScroller_(false);
	~time_view.hasVerticalScroller_(false);
	~time_view.editable = false;
	~time_view.background = Color.grey(0.6);
	~time_view.stringColor = Color.grey(0.85);
	~time_view.font = Font("Helvetica", 150);
	htop.add(~time_view);

	"adding timer start button\n".postf();
	~start_timer_gui = Button(sooperlooper, Rect(0, 0, gui_button_size * 2, gui_button_size * 2));
	~start_timer_gui.states_([[" START TIMER ", Color.white, Color.red],[" STOP TIMER  ", Color.white, Color.grey]]);

	// start elapsed timer
	~start_timer_gui.action_({ | but |
		if (but.value == 1, {
			~start_elapsed_timer = true;
			"GUI: start timer %\n".postf(but.value);
		}, {
			~start_elapsed_timer = false;
			"GUI: reset timer %\n".postf(but.value);
		});
	});
	htop.add(40);
	htop.add(~start_timer_gui, align: \top);

	vmaster.add(htop, stretch: 1, align: \center);

	// create input strips
	~input_count.do ({ | input |
		// strip layout
		var vlay = VLayout.new();
		// fader, level meter and balance side by side
		var hfad = HLayout.new();

		"adding input spectral bend knob %\n".postf(input);
		~input_spectral_bend_gui[input] = Knob(sooperlooper, (gui_button_size @ gui_button_size) * 1.0);
		// this is wrong, colors are just four for knobs FIXME
		~input_spectral_bend_gui[input].color(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~input_spectral_bend_gui[input].centered = true;

		// input spectral bend knob action
		~input_spectral_bend_gui[input].action_({ | knob |
			~input_spectral_bend_update.(input, knob.value.bilin(0.5, 0, 1, 0, -1, 1), \gui);
			"GUI: input % bend: %\n".postf(input, ~input_spectral_bend_state[input]);
		});
		vlay.add(~input_spectral_bend_gui[input], stretch: 1);

		"adding input trajectory view %\n".postf(input);
		~input_trajectory_gui[input] = Slider2D.new(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~input_trajectory_gui[input].setXY(0.5, 0.5);
		~input_trajectory_gui[input].knobColor = Color.red(0.7);
		vlay.add(~input_trajectory_gui[input]);

		"adding input channel fader %\n".postf(input);
		~input_level_gui[input] = EZSlider(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len), "LEV",
			initVal: ~input_level_state[input], unitWidth: gui_button_size, numberWidth: gui_button_size, layout: \vert);
		~input_level_gui[input].setColors(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, Color.blue(0.5), Color.grey(0.7));
		~input_level_gui[input].sliderView.focusColor_(Color.clear);
		~input_level_gui[input].font_(Font("Helvetica",18));
		~input_level_gui[input].sliderView.thumbSize = 50;

		// input level fader action
		~input_level_gui[input].action_({ | fader |
			~input_level_update.(input, ~fader_value_to_amp.(fader.value, value_min: 0.0, value_max: 1.0), \gui);
			"GUI: input % level: %, fader: %\n".postf(input, ~input_level_state[input], fader.value);
		});
		hfad.add(~input_level_gui[input].view);

		"adding balance fader %\n".postf(input);
		~input_balance_gui[input] = EZSlider(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len), "BAL",
			initVal: ~input_balance_state[input], unitWidth: gui_button_size, numberWidth: gui_button_size, layout: \vert);
		~input_balance_gui[input].setColors(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~input_balance_gui[input].sliderView.focusColor_(Color.clear);
		~input_balance_gui[input].font_(Font("Helvetica",18));
		~input_balance_gui[input].sliderView.thumbSize = 50;

		// input level or balance fader action
		~input_balance_gui[input].action_({ | fader |
			~input_balance_update.(input, ~fader_value_to_amp.(fader.value, dB_offset: -10, value_min: 0.0, value_max: 1.0), \gui);
			"GUI: input % balance: %, fader: %\n".postf(input, ~input_balance_state[input], fader.value);
		});
		hfad.add(~input_balance_gui[input].view);

		"adding input level meter %\n".postf(input);
		~input_meter_gui[input] = LevelIndicator(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len));
		~input_meter_gui[input].style = \led;
		~input_meter_gui[input].numSteps = 512;
		~input_meter_gui[input].drawsPeak = true;
		~input_meter_gui[input].numSteps = 10;
		~input_meter_gui[input].numTicks = 11;
		~input_meter_gui[input].numMajorTicks = 3;
		~input_meter_gui[input].background = Color.grey(0.5);
		hfad.add(~input_meter_gui[input]);

		vlay.add(hfad, stretch: 8);

		"adding mute button %\n".postf(input);
		~input_mute_gui[input] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~input_mute_gui[input].states_([["MUTE", Color.white, Color.grey],["MUTE", Color.white, Color.red]]);

		// input recording button action
		~input_mute_gui[input].action_({ | but |
			~input_mute_update.(input, 1 - (but.value), \gui);
			"GUI: input % mute %\n".postf(input, but.value);
		});
		vlay.add(~input_mute_gui[input], stretch: 1);

		// add input strip
		hlay.add(vlay, stretch: 1);
	});

	// add space between inputs and loopers
	hlay.add(20);

	//
	// create all loop control strips
	//
	~loop_count.do({ | looper |

		// create a channel strip vertical layout
		var vlay = VLayout.new();
		// fader, level meter and balance side by side
		var hfad = HLayout.new();
		// rate and bender knobs side by side
		var hrb = HLayout.new();
		
		"adding rate center button %\n".postf(looper);
		~loop_rate_center_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_rate_center_gui[looper].states_([["CENTER", Color.white, Color.grey],["CENTER", Color.white, Color.grey]]);

		// rate value center button action
		~loop_rate_center_gui[looper].action_({ | but |
			~loop_rate_update.(looper, 1.0, \gui);
			"GUI: loop % rate center\n".postf(looper);
		});
		vlay.add(~loop_rate_center_gui[looper]);

		"adding rate knob %\n".postf(looper);
		~loop_rate_gui[looper] = Knob(sooperlooper, (gui_button_size @ gui_button_size) * 1.0);
		~loop_rate_gui[looper].color(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~loop_rate_gui[looper].centered = true;

		// rate knob action
		~loop_rate_gui[looper].action_({ | knob |
			~loop_rate_update.(looper, knob.value.bilin(0.5, 0.0, 1.0, 1.0, 0.125, 4.0), \gui);
			"GUI: loop % rate: % (%)\n".postf(looper, ~loop_rate_state[looper], ~loop_rate_sign_state[looper]);
		});
		hrb.add(~loop_rate_gui[looper]);

		"adding spectral bend knob %\n".postf(looper);
		~loop_spectral_bend_gui[looper] = Knob(sooperlooper, (gui_button_size @ gui_button_size) * 1.0);
		~loop_spectral_bend_gui[looper].color(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~loop_spectral_bend_gui[looper].centered = true;

		// rate knob action
		~loop_spectral_bend_gui[looper].action_({ | knob |
			~loop_spectral_bend_update.(looper, knob.value.bilin(0.5, 0, 1, 0, -1, 1), \gui);
			"GUI: loop % bend: %\n".postf(looper, ~loop_spectral_bend_state[looper]);
		});
		hrb.add(~loop_spectral_bend_gui[looper]);
		vlay.add(hrb, stretch: 1);

		"adding trajectory view %\n".postf(looper);
		~loop_trajectory_gui[looper] = Slider2D.new(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_trajectory_gui[looper].knobColor = Color.red(0.7);
		~loop_trajectory_gui[looper].setXY(0.5, 0.5);
		vlay.add(~loop_trajectory_gui[looper]);

		"adding loop status and restart light %\n".postf(looper);
		~loop_status_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_status_gui[looper].states_([[(looper+1).asString, Color.white, Color.grey],
			[(looper+1).asString, Color.white, Color.green],
			[(looper+1).asString, Color.white, Color.red]]);

		// loop restart button action
		~loop_status_gui[looper].action_({ | but |
			~loop_restart_update.(looper, but.value, \gui);
			"GUI: loop % restart %\n".postf(looper, but.value);
		});
		vlay.add(~loop_status_gui[looper]);

		"adding sync button %\n".postf(looper);
		~loop_sync_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_sync_gui[looper].states_([["SYNC", Color.white, Color.grey],["SYNC", Color.white, Color.blue]]);
		~loop_sync_gui[looper].value = ~loop_sync_state[looper];

		// loop sync action
		~loop_sync_gui[looper].action_({ | but |
			var value = but.value;
			if (value == 1 and: { ~loop_sync_source_state[looper] != 0 }, {
				// we cannot set source and destination of sync to the same looper
				value = 0;
			});
			~loop_sync_update.(looper, value, \gui);
			"GUI: loop % sync %\n".postf(looper, value);
		});
		vlay.add(~loop_sync_gui[looper]);

		"adding source A button %\n".postf(looper);
		~loop_rec_source_A_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_rec_source_A_gui[looper].states_([["INPUT A", Color.white, Color.grey],["INPUT A", Color.white, Color.blue(0.3)]]);
		~loop_rec_source_A_gui[looper].value = ~loop_rec_source_A_state[looper];

		// loop source A action
		~loop_rec_source_A_gui[looper].action_({ | but |
			~loop_rec_source_A_update.(looper, but.value, \gui);
			"GUI: loop % rec source A %\n".postf(looper, but.value);
		});
		vlay.add(~loop_rec_source_A_gui[looper]);

		"adding source B button %\n".postf(looper);
		~loop_rec_source_B_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_rec_source_B_gui[looper].states_([["INPUT B", Color.white, Color.grey],["INPUT B", Color.white, Color.blue(0.3)]]);
		~loop_rec_source_B_gui[looper].value = ~loop_rec_source_B_state[looper];

		// loop source B action
		~loop_rec_source_B_gui[looper].action_({ | but |
			~loop_rec_source_B_update.(looper, but.value, \gui);
			"GUI: loop % rec source B %\n".postf(looper, but.value);
		});
		vlay.add(~loop_rec_source_B_gui[looper]);

		"adding reverse button %\n".postf(looper);
		~loop_reverse_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_reverse_gui[looper].states_([["REV", Color.white, Color.grey],["REV", Color.white, Color.green]]);
		~loop_reverse_gui[looper].value = if (~loop_rate_sign_state[looper] < 0, { 1 }, { 0 });

		// loop reverse action
		~loop_reverse_gui[looper].action_({ | but |
			~loop_rate_sign_update.(looper, but.value, \gui);
			"GUI: loop % reverse %\n".postf(looper, but.value);
		});
		vlay.add(~loop_reverse_gui[looper]);

		"adding overdubbing button %\n".postf(looper);
		~loop_dub_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_dub_gui[looper].states_([["DUB", Color.white, Color.grey],["DUB", Color.white, Color.red]]);

		// loop overdub action
		~loop_dub_gui[looper].action_({ | but |
			~loop_dub_update.(looper, but.value, \gui);
			"GUI: loop % overdub %\n".postf(looper, but.value);
		});
		vlay.add(~loop_dub_gui[looper]);

		"adding channel fader %\n".postf(looper);
		~loop_level_gui[looper] = EZSlider(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len), "LEV",
			initVal: ~loop_level_state[looper], unitWidth: gui_button_size, numberWidth: gui_button_size, layout: \vert);
		~loop_level_gui[looper].setColors(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~loop_level_gui[looper].sliderView.focusColor_(Color.clear); 
		~loop_level_gui[looper].font_(Font("Helvetica",18));
		~loop_level_gui[looper].sliderView.thumbSize = 50;

		// loop level or balance fader action
		~loop_level_gui[looper].action_({ | fader |
			~loop_level_update.(looper, ~fader_value_to_amp.(fader.value, value_min: 0.0, value_max: 1.0), \gui);
			"GUI: loop % level: %, fader: %\n".postf(looper, ~loop_level_state[looper], fader.value);
		});
		hfad.add(~loop_level_gui[looper].view, stretch: 2);

		"adding balance fader %\n".postf(looper);
		~loop_balance_gui[looper] = EZSlider(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len), "BAL",
			initVal: ~loop_balance_state[looper], unitWidth: gui_button_size, numberWidth: gui_button_size, layout: \vert);
		~loop_balance_gui[looper].setColors(Color.grey, Color.white, Color.grey(0.7), Color.grey, Color.white, Color.yellow, nil, nil, Color.grey(0.7));
		~loop_balance_gui[looper].sliderView.focusColor_(Color.clear);
		~loop_balance_gui[looper].font_(Font("Helvetica",18));
		~loop_balance_gui[looper].sliderView.thumbSize = 50;

		// loop level or balance fader action
		~loop_balance_gui[looper].action_({ | fader |
			~loop_balance_update.(looper, ~fader_value_to_amp.(fader.value, dB_offset: -10, value_min: 0.0, value_max: 1.0), \gui);
			"GUI: loop % balance: %, fader: %\n".postf(looper, ~loop_balance_state[looper], fader.value);
		});
		hfad.add(~loop_balance_gui[looper].view, stretch: 2);

		"adding level meter %\n".postf(looper);
		~loop_meter_gui[looper] = LevelIndicator(sooperlooper, Rect(0, 0, gui_button_size, gui_fader_len));
		~loop_meter_gui[looper].style = \led;
		~loop_meter_gui[looper].numSteps = 512;
		~loop_meter_gui[looper].drawsPeak = true;
		~loop_meter_gui[looper].numSteps = 10;
		~loop_meter_gui[looper].numTicks = 11;
		~loop_meter_gui[looper].numMajorTicks = 3;
		~loop_meter_gui[looper].background = Color.grey(0.5);
		hfad.add(~loop_meter_gui[looper], stretch: 2);

		"adding playback location display %\n".postf(looper);
		~loop_location_gui[looper] = LevelIndicator(sooperlooper, Rect(0, 0, gui_button_size / 2, gui_fader_len));
		~loop_location_gui[looper].style = \continuous;
		~loop_location_gui[looper].numSteps = 10;
		~loop_location_gui[looper].numTicks = 11;
		~loop_location_gui[looper].numMajorTicks = 3;
		~loop_location_gui[looper].background = Color.grey(0.5);
		~loop_location_gui[looper].meterColor = Color.blue(0.5);
		~loop_location_gui[looper].warningColor = Color.blue(0.5);
		~loop_location_gui[looper].criticalColor = Color.blue(0.5);
		hfad.add(~loop_location_gui[looper], stretch: 1);

		vlay.add(hfad, stretch: 8);

		"adding recording button %\n".postf(looper);
		~loop_rec_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_rec_gui[looper].states_([["REC", Color.white, Color.grey],["REC", Color.white, Color.red]]);

		// loop recording button action
		~loop_rec_gui[looper].action_({ | but |
			~loop_rec_update.(looper, but.value, \gui);
			"GUI: loop % rec %\n".postf(looper, but.value);
		});
		vlay.add(~loop_rec_gui[looper], stretch: 1);
		
		"adding mute button %\n".postf(looper);
		~loop_mute_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_mute_gui[looper].states_([["MUTE", Color.white, Color.grey],["MUTE", Color.white, Color.red]]);

		// loop recording button action
		~loop_mute_gui[looper].action_({ | but |
			~loop_mute_update.(looper, 1 - (but.value), \gui);
			"GUI: loop % mute %\n".postf(looper, but.value);
		});
		vlay.add(~loop_mute_gui[looper], stretch: 1);

		"add vlay %\n".postf(vlay);
		hlay.add(vlay, stretch: 1);

		// add space between two banks of loopers
		if (looper == 7, {
			hlay.add(20);
		});
	});

	// add input and looper strips
	vmaster.add(hlay, stretch: 8);

	// create sync source interface
	~loop_count.do ({ | looper |
		"adding sync source button %\n".postf(looper);
		~loop_sync_source_gui[looper] = Button(sooperlooper, Rect(0, 0, gui_button_size, gui_button_size));
		~loop_sync_source_gui[looper].states_([[(looper+1).asString, Color.white, Color.grey],[(looper+1).asString, Color.white, Color.blue]]);

		// loop recording button action
		~loop_sync_source_gui[looper].action_({ | but |
			var value = but.value;
			if (value == 1 and: { ~loop_sync_state[looper] != 0 }, {
				// we cannot set source and destination of sync to the same looper
				value = 0;
			});
			~loop_sync_source_update.(looper, value, \gui);
			"GUI: loop % sync source %\n".postf(looper, value);
		});
		blay.add(~loop_sync_source_gui[looper], stretch: 1);
	});

	// add to vertical layout
	vmaster.add(30);
	vmaster.add(blay, stretch: 1);

	// add channel strips to layout
	sooperlooper.layout = vmaster;

	// start in layer A
	~midi_layer = 0;
	// update existing state for all looper strips in surface
	~strip_mode_update.(\strip_all, 1);
	~strip_mode_update.(\strip_all, 0);
	// update existing state for all inputs
	~input_count.do ({ | input |
		// inputs
		//
		// input spectral bend knob
		~input_spectral_bend_update.(input, ~input_spectral_bend_state[input], nil);
		// input level fader
		~input_level_update.(input, ~input_level_state[input], nil);
		// input balance fader
		~input_balance_update.(input, ~input_balance_state[input], nil);
		// input mute button
		~input_mute_update.(input, ~input_mute_state[input], nil);
	});
	// update existing state for all layer shared controls
	~layer_count.do ({ | layer |
		~strip_mode_update.(\strip_all, layer, nil);
		// trajectory k X offset
		~trajectory_kx_update.(layer, ~trajectory_kx_state[layer], nil);
		// trajectory k Y offset
		//~trajectory_ky_update.(layer, ~trajectory_ky_state[layer], nil);
		// REUSE KNOB FOR NOW...
		~layer_overlap_update.(layer,
			if (layer == 0, {
				~loop_overlap_state[0];
			}, {
				~loop_overlap_state[8];
			}), nil);
		// trajectory size multiplier
		~trajectory_size_update.(layer, ~trajectory_size_state[layer], nil);
		// trajectory speed multiplier
		~trajectory_speed_update.(layer, ~trajectory_speed_state[layer], nil);
		// loop reverb amount
		~loop_reverb_amount_update.(layer, ~loop_reverb_amount_state[layer], nil);
		// loop reverb time
		~layer_reverb_time_update.(layer, ~layer_reverb_time_state[layer], nil);
	});
	~loop_count.do ({ | looper |
		// loop sync source
		~loop_sync_source_update.(looper, ~loop_sync_source_state[looper], nil);
	});
	// start elapsed timer button
	~start_timer_gui.value = if (~start_elapsed_timer == true, { 1 }, { 0 });

	// clean up when window is closed
	sooperlooper.onClose({
		if (~time_view.notNil, {
			~time_view.free;
			~time_view = nil;
		});
		if (~start_timer_gui.notNil, {
			~start_timer_gui.free;
			~start_timer_gui = nil;
		});
		~input_count.do ({ | input |
			if (~input_spectral_bend_gui[input].notNil, {
				~input_spectral_bend_gui[input].free;
				~input_spectral_bend_gui[input] = nil;
			});
			if (~input_trajectory_gui[input].notNil, {
				~input_trajectory_gui[input].free;
				~input_trajectory_gui[input] = nil;
			});
			if (~input_level[input].notNil, {
				~input_level[input].free;
				~input_level[input] = nil;
			});
			if (~input_balance[input].notNil, {
				~input_balance[input].free;
				~input_balance[input] = nil;
			});
			if (~input_meter[input].notNil, {
				~input_meter[input].free;
				~input_meter[input] = nil;
			});
			if (~input_mute[input].notNil, {
				~input_mute[input].free;
				~input_mute[input] = nil;
			});
		});
		~loop_count.do ({ | looper |
			if (~loop_rate_center_gui[looper].notNil, {
				~loop_rate_center_gui[looper].free;
				~loop_rate_center_gui[looper] = nil;
			});
			if (~loop_rate_gui[looper].notNil, {
				~loop_rate_gui[looper].free;
				~loop_rate_gui[looper] = nil;
			});
			if (~loop_spectral_bend_gui[looper].notNil, {
				~loop_spectral_bend_gui[looper].free;
				~loop_spectral_bend_gui[looper] = nil;
			});
			if (~loop_trajectory_gui[looper].notNil, {
				~loop_trajectory_gui[looper].free;
				~loop_trajectory_gui[looper] = nil;
			});
			if (~loop_status_gui[looper].notNil, {
				~loop_status_gui[looper].free;
				~loop_status_gui[looper] = nil;
			});
			if (~loop_sync_gui[looper].notNil, {
				~loop_sync_gui[looper].free;
				~loop_sync_gui[looper] = nil;
			});
			if (~loop_rec_source_A[looper].notNil, {
				~loop_rec_source_A[looper].free;
				~loop_rec_source_A[looper] = nil;
			});
			if (~loop_rec_source_B[looper].notNil, {
				~loop_rec_source_B[looper].free;
				~loop_rec_source_B[looper] = nil;
			});
			if (~loop_reverse_gui[looper].notNil, {
				~loop_reverse_gui[looper].free;
				~loop_reverse_gui[looper] = nil;
			});
			if (~loop_dub_gui[looper].notNil, {
				~loop_dub_gui[looper].free;
				~loop_dub_gui[looper] = nil;
			});
			if (~loop_level_gui[looper].notNil, {
				~loop_level_gui[looper].free;
				~loop_level_gui[looper] = nil;
			});
			if (~loop_balance_gui[looper].notNil, {
				~loop_balance_gui[looper].free;
				~loop_balance_gui[looper] = nil;
			});
			if (~loop_meter_gui[looper].notNil, {
				~loop_meter_gui[looper].free;
				~loop_meter_gui[looper] = nil;
			});
			if (~loop_location_gui[looper].notNil, {
				~loop_location_gui[looper].free;
				~loop_location_gui[looper] = nil;
			});
			if (~loop_rec_gui[looper].notNil, {
				~loop_rec_gui[looper].free;
				~loop_rec_gui[looper] = nil;
			});
			if (~loop_mute_gui[looper].notNil, {
				~loop_mute_gui[looper].free;
				~loop_mute_gui[looper] = nil;
			});
		});
	});
};

//
// Elapsed time display task
//

~start_elapsed_timer = false;

~elapsed_timer_task = nil;

~start_timer = {
	if (~elapsed_timer_task.isNil, {
		Task({
			var elapsed;
			var hours, minutes, seconds, rest;
			var start_time = nil;
			inf.do({
				if (~start_elapsed_timer and: { start_time.notNil }, {
					elapsed = thisThread.seconds - start_time;
				}, {
					elapsed = 0;
					start_time = thisThread.seconds;
				});
				# hours, minutes, seconds, rest = elapsed.asTimeString(1.0).split($:);
				{
					// update elapsed time
					if (~time_view.notNil, {
						~time_view.string = (hours++":"++minutes++":"++seconds.asInteger.asString.padLeft(2, "0"));
						~time_view.stringColor = Color.grey(0.85);
					});
				}.defer;
				// update once a second
				1.0.wait;
			});
		}).play(SystemClock);
	}, {
		// timer already running, stop it
		~start_elapsed_timer = false;
	});
};

//
// Initialize X-Touch Compact and SC GUI to current state
//

~initialize_surface = {
	var global_ch = 1;

	[\gui, \midi].do ({ | src |
		~input_count.do ({ | input |
			// inputs
			//
			// input spectral bend knob
			~input_spectral_bend_update.(input, ~input_spectral_bend_state[input], src);
			// input level fader
			~input_level_update.(input, ~input_level_state[input], src);
			// input balance fader
			~input_balance_update.(input, ~input_balance_state[input], src);
			// input mute button
			~input_mute_update.(input, ~input_mute_state[input], src);
		});

		~layer_count.do ({ | layer |
			~strip_mode_update.(\strip_all, layer, src);
			// trajectory k X offset
			~trajectory_kx_update.(layer, ~trajectory_kx_state[layer], src);
			// trajectory k Y offset
			~trajectory_ky_update.(layer, ~trajectory_ky_state[layer], src);
			// trajectory size multiplier
			~trajectory_size_update.(layer, ~trajectory_size_state[layer], src);
			// trajectory speed multiplier
			~trajectory_speed_update.(layer, ~trajectory_speed_state[layer], src);
			// loop reverb amount
			~loop_reverb_amount_update.(layer, ~loop_reverb_amount_state[layer], src);
			// loop reverb time
			~layer_reverb_time_update.(layer, ~layer_reverb_time_state[layer], src);
		});

		~loop_count.do ({ | looper |
			// loops
			//
			// loop restart indicator
			~loop_restart_off.(looper, true);
			// loop rate knob
			~loop_rate_update.(looper, ~loop_rate_state[looper], src);
			// loop spectral bend knob
			~loop_spectral_bend_update.(looper, ~loop_spectral_bend_state[looper], src);
			// loop status button has no state (normally off in surface)
			//
			// loop reverse button
			~loop_rate_sign_update.(looper, ~loop_rate_sign_state[looper], src);
			// loop overdub button
			~loop_dub_update.(looper, ~loop_dub_state[looper], src);
			// loop level fader
			~loop_level_update.(looper, ~loop_level_state[looper], src);
			// loop balance fader
			~loop_balance_update.(looper, ~loop_balance_state[looper], src);
			// loop record button
			~loop_rec_update.(looper, ~loop_rec_state[looper], src);
			// loop playback mute button
			~loop_mute_update.(looper, ~loop_mute_state[looper], src);
			// loop playback sync button
			~loop_sync_update.(looper, ~loop_sync_state[looper], src);
			// loop source A
			~loop_rec_source_A_update.(looper, ~loop_rec_source_A_state[looper], src);
			// loop sourcec B
			~loop_rec_source_B_update.(looper, ~loop_rec_source_B_state[looper], src);
		});
	});
	// go back to layer A
	~midi_set_layer.(0);
};

//
// Configure behavior of X-Touch Compact
//

~configure_surface = { | g_chan = 0 |
	if (~midi_out.notNil, {
		// global mode: normal
		~midi_out.control(g_chan, 127, 0);
	});
	// knob led ring behavior
	[false, true].do ({ | layer |
		~midi_set_layer.(layer);
		16.do ({ | i |
			~midi_out.control(g_chan, 10 + i, 0);
		});
	});
};

// start input processors

~start_input_processing = {
	~input_count.do ({ | input |
		// start the input processor
		if (~inputs[input].notNil, {
			~inputs[input].free;
		});
		~inputs[input] = Synth(\input_2o, [\input, input, \output, 0, \reverb, ~reverb_bus[input].index,
			\master, ~master_level_state, \level, ~loop_level_state[input], \balance, ~loop_balance_state[input],
			\mute, ~loop_mute_state[input], \reverb_amount, ~loop_reverb_amount_state[input],
			\vector, ~pan_control_buses[input + ~loop_count],
			// spectral processing
			\mod_freq, (0.21 + 0.06.sum3rand),
			\polarity, if (0.5.coin, { 1 }, { -1 })],
			~gen_group);
	});
};

~stop_input_processing = {
	~input_count.do ({ | input |
		// stop input processor
		if (~inputs[input].notNil, {
			~inputs[input].free;
			~inputs[input] = nil;
		});
	});
};

// start master sync clock task

~master_sync_clock_task = nil;
~master_sync_clock_period = 5.0;

~start_master_sync_clock = {
	~master_sync_clock_task = Task({
		inf.do({
			~loop_count.do ({ | looper |
				if (~loop_sync_state[looper] == 1, {
					if (~loopers[looper].isNil, {
						// synth is nil, start it
						~looper_start_playing.(looper);
						"CLOCK: start looper %\n".postf(looper);
					});
				});
			});
			// "CLOCK: wait % ...\n".postf(~master_sync_clock_period);
			// wait till the next sync
			~master_sync_clock_period.wait;
		});
	}).play(SystemClock);
};

~stop_master_sync_clock = {
	~master_sync_clock_task.stop;
};

// start reverberation engine

~start_reverb = {
	~layer_count.do ({ | layer |
		if (~reverb[layer].isNil, {
			~reverb[layer] = Synth.new(\AmbiRev, [
				\input, ~reverb_bus[layer].index,
				\output, 0,
				\gain, 1.0],
				~rev_group);
		}, {
			"ERROR: reverb instrument already running in layer %! (%)\n".postf(layer, ~reverb);
		});
	});
};

~stop_reverb = {
	~layer_count.do ({ | layer |
		if (~reverb[layer].notNil, {
			~reverb[layer].free;
			~reverb[layer] = nil;
		}, {
			"ERROR: reverb instrument not running in layer %! (%)\n".postf(layer, ~reverb);
		});
	});
};
)


~start_pan_controllers.();

~start_input_processing.();

~create_gui.();

~start_timer.();

~start_reverb.();

)

~initialize_surface.();

~start_master_sync_clock.();

~master_sync_clock_period = 1.0;

~stop_reverb.();

~stop_master_sync_clock.();

~stop_input_processing.();

~start_pan_controllers.();

~stop_pan_controllers.();

"====================================".postln;

~loopers[0].set(\run, 0);

~loopers[0].get(\gate, { | value | value.postln; });

~loopers[0].set(\azOffset, -0);

~pan_control_buses[0].get;

~pan_control_buses[0] = 0;

~pan_control_buses[0].get;

~pan_controllers[0].set(\x_k_i, 2);
~pan_controllers[0].set(\y_k_i, 2);

~pan_controllers[0].set(\x_k, 0);
~pan_controllers[0].set(\y_k, 0);

~pan_controllers[0].set(\y_k, 2.5);
~pan_controllers[1].set(\y_k, 2.5);
~pan_controllers[2].set(\y_k, 2.5);
~pan_controllers[3].set(\y_k, 2.5);
~pan_controllers[4].set(\y_k, 2.5);

~pan_controllers[0].set(\x_phase, 0);
~pan_controllers[0].set(\y_phase, 0);

~pan_controllers[0].set(\z_scale, 0);

(~pan_controllers[0]).set(\y_scale, 1.5);

(~pan_controllers[0]).set(\x_scale, 0);

~pan_controllers[0].set(\speed, 1.5);

~pan_controllers[0].set(\direction, -1);

~pan_controllers[0].set(\size, 10);
~pan_controllers[1].set(\size, 10);

~pan_controllers[0].get(\y_k, { | v | v.postln; });

~pan_controllers[0].get(\direction, { | v | v.postln; });

~pan_controllers[3].get(\bus, { | v | v.postln; });

~pan_controllers[8].set(\x_k, pi);

~loop_count.do ({ | l | ~pan_controllers[l].set(\z_scale, 0.666); });
